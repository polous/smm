define(["text!tpl/tab.toolbox.group.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			$(this).find(".uiparam-option").draggable({
				revert: true,
				revertDuration: 150,
				appendTo: 'body',
				containment: 'window',
				helper: 'clone',
				scroll: false,
				zIndex: 99999
			});
		}, {
		},
		function(view) {
			var alteredView = {
				"name": view.name,
				"label": view.label,
				"options": []
			};
			iterateObjectSorted(view.options, function(optionName, option) {
				alteredView.options.push({
					"optionName": optionName,
					"optionLabel": option.label
				});
			}, function(a, b) {
				return view.options[a].sortIndex - view.options[b].sortIndex;
			});
			return alteredView;
		}
	);

});