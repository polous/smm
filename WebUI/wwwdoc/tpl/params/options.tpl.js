define(["text!tpl/params/options.tpl.html", "tpl/tab.main.param.tpl.js"], function(tpl, mainParamTpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;

			this.view = view;

			$(this).find(".uiparam-group").droppable({
			  greedy: true,
			  activeClass: "active",
			  accept: function(draggable) {
				return draggable.data("uiparam-group") == view.optionGroup;
			  },
			  drop: function(event, ui) {
			  	self.addParam(ui.draggable.data("uiparam-group"), ui.draggable.data("uiparam-option"));
			  }
			});

			if (view.default != null)
			for (var i=0; i<view.default.length; i++) {
				this.addParam(view.optionGroup, view.default[i]);
			}
		}, {
			params: [],

			addParam: function(group, option) {
				var self = this;
				
				if (this.view.optionCountMax!=null && this.params.length >= this.view.optionCountMax) {
			  		if (self.params.length > 0) {
				  		$(self.params.shift()).remove();
				  	}
			  	}

			  	if (this.view.optionCountMax==null || this.params.length < this.view.optionCountMax) {
			  		var param = mainParamTpl.render(UIPARAMS_OPTIONS[group].options[option]);
			  		self.params.push(param);
			  		$(param).on("close", function(event) {
			  			for (var i=0; i<self.params.length; i++) {
			  				if (self.params[i] == param) {
			  					self.params.splice(i, 1);
			  					param.remove();
			  				}
			  			}
			  			event.stopPropagation();
			  		});
			  		$(self).find(".uiparam-group-params").first().append(param);
			  	}
			},

			getValue: function() {
				var params = [];

				for (var uiParamGroupParamParamI=0; uiParamGroupParamParamI<this.params.length; uiParamGroupParamParamI++) {
					var uiParamGroupParamParam = this.params[uiParamGroupParamParamI];
					
					params.push({"name": uiParamGroupParamParam.name, "params": uiParamGroupParamParam.getValue()});
				}

				return {
					"optionGroup": this.view.optionGroup,
					"params": params
				};
			}
		}
	);

});