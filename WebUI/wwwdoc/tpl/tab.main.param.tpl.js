define(["text!tpl/tab.main.param.tpl.html", "tpl/tab.main.param.param.tpl.js"], function(tpl, mainParamParamTpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;

			this.name = view.name;
			iterateObjectSorted(view.params, function(paramName, param) {
				var paramParam = mainParamParamTpl.render(param);
				self.params.push(paramParam);
				$(self).find(".uiparam-params").first().append(paramParam);
			}, function(a, b) {
				return view.params[a].sortIndex - view.params[b].sortIndex;
			});

			$(this).find(".uiparam-bt-close").first().button({
				icons: {
					primary: "ui-icon-close"
				},
				text: false
			}).click(function(event, ui) {
				$(self).trigger("close");
				event.stopPropagation();
			});
		}, {
			name: null,
			params: [],

			getValue: function() {
				var params = {};

				for (var uiParamGroupParamParamI=0; uiParamGroupParamParamI<this.params.length; uiParamGroupParamParamI++) {
					var uiParamGroupParamParam = this.params[uiParamGroupParamParamI];
					params[uiParamGroupParamParam.name] = uiParamGroupParamParam.getValue();
				}

				return params;
			}
		}
	);

});