define(["text!tpl/tab.tpl.html", "tpl/tab.toolbox.group.tpl.js", "tpl/tab.main.group.tpl.js", "tpl/tab.result.event.tpl.js", "tpl/tab.result.dialog.tpl.js"], function(tpl, toolboxGroupTpl, mainGroupTpl, resultEventTpl, resultDialogTpl) {
	var CONF_EXEC_UPDATE_INTERVAL = 1000;

	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;
			this.view = view;

			// set up splitter
			$(this).find(".Splitter1").splitter({
				type: "v",
				//outline: true,
				minLeft: $(this).find(".SplitterLeft").css("min-width")!=""?parseInt($(this).find(".SplitterLeft").css("min-width")):null,
				maxLeft: $(this).find(".SplitterLeft").css("max-width")!=""?parseInt($(this).find(".SplitterLeft").css("max-width")):null,
				//resizeToWidth: true,
			});
			$(this).find(".Splitter2").splitter({
				type: "v",
				//outline: true,
				minLeft: $(this).find(".SplitterMiddle").css("min-width")!=""?parseInt($(this).find(".SplitterMiddle").css("min-width")):null,
				maxLeft: $(this).find(".SplitterMiddle").css("max-width")!=""?parseInt($(this).find(".SplitterMiddle").css("max-width")):null,
				minRight: $(this).find(".SplitterRight").css("min-width")!=""?parseInt($(this).find(".SplitterRight").css("min-width")):null,
				maxRight: $(this).find(".SplitterRight").css("max-width")!=""?parseInt($(this).find(".SplitterRight").css("max-width")):null,
				//resizeToWidth: true,
			});
			var preventRecursion=false;
			$(window).resize(function() {
				if (preventRecursion)
					return;

				preventRecursion = true;
				$(self).find(".Splitter1").css("width", $(self).width()).css("height", $(self).height()).trigger("resize");
				preventRecursion = false;
			});
			setTimeout(function() {
				$(self).find(".Splitter1").css("width", $(self).width()).css("height", $(self).height()).trigger("resize");
			}, 100);

			// render uiParam options
			iterateObjectSorted(view.uiparam_options, function(uiParamOptionsGroupName, uiParamOptionGroup) {
				$(self).find(".view-toolbox .uiparam-options-groups").append(toolboxGroupTpl.render(uiParamOptionGroup));
			}, function(a, b) {
				return view.uiparam_options[a].sortIndex - view.uiparam_options[b].sortIndex;
			});

			// render uiParam groups
			iterateObjectSorted(view.uiparams, function(uiParamGroupName, uiParamGroup) {
				var paramGroup = mainGroupTpl.render(uiParamGroup);
				for (var i=0; i<uiParamGroup["default"].length; i++) {
					paramGroup.addParam(uiParamGroup["optionGroup"], uiParamGroup["default"][i]);
				}

				self.uiParamGroups.push(paramGroup);
				$(self).find(".view-main .uiparam-groups").append(paramGroup);
			}, function(a, b) {
				return view.uiparams[a].sortIndex - view.uiparams[b].sortIndex;
			});
			if (self.uiParamGroups.length > 0)
				$(self.uiParamGroups[0]).find(".ui-widget-header").click();

			// attach to "run" button
			var btRun = $(this).find("button.bt-run").button({
				icons: {
					primary: "ui-icon-play"
				}
			}).click(function() {
				self.exec();
			});

			// attach to "plot" button
			var btPlot = $(this).find(".bt-plot").button({
				icons: {
					primary: "ui-icon-calculator"
				},
				text: false,
				disabled: true
			}).tooltip({
				track: true, 
				position: {my: "left top+25", at: "left bottom", collision: "flipfit"}
			});
			btPlot.click(function() {
				self._openPlot(resultDialogTpl);
			});

			// attach to "csv" button
			var btCsv = $(this).find(".bt-csv").button({
				icons: {
					primary: "ui-icon-arrowthickstop-1-s"
				},
				text: false,
				disabled: true
			}).tooltip({
				track: true,
				position: {my: "left top+25", at: "left bottom", collision: "flipfit"}
			});
		}, {
			runId: null,
			runResult: null,
			uiParamGroups: [],

			exec: function() {
				var self = this;

				// assemble parameters
				var params = {};
				for (var uiParamGroupI=0; uiParamGroupI<self.uiParamGroups.length; uiParamGroupI++) {
					var uiParamGroup = self.uiParamGroups[uiParamGroupI];
					params[uiParamGroup.name] = [];

					if (UIPARAMS[uiParamGroup.name].optionCountMin == UIPARAMS[uiParamGroup.name].optionCountMax
						&& UIPARAMS[uiParamGroup.name].optionCountMin != null
						&& UIPARAMS[uiParamGroup.name].optionCountMin != uiParamGroup.params.length) {
						this.notifyError("Missing parameter: '" + UIPARAMS[uiParamGroup.name].label + "'");
						return;
					} else if (UIPARAMS[uiParamGroup.name].optionCountMin != null && uiParamGroup.params.length < UIPARAMS[uiParamGroup.name].optionCountMin) {
						this.notifyError("Too few parameters: '" + UIPARAMS[uiParamGroup.name].label+ "'. At least " + UIPARAMS[uiParamGroup.name].optionCountMin + " necessary.");
						return;
					} else if (UIPARAMS[uiParamGroup.name].optionCountMax != null && uiParamGroup.params.length > UIPARAMS[uiParamGroup.name].optionCountMax) {
						this.notifyError("Too many parameters: '" + UIPARAMS[uiParamGroup.name].label+ "'. At max " + UIPARAMS[uiParamGroup.name].optionCountMin + " possible.");
						return;
					}

					for (var uiParamGroupParamI=0; uiParamGroupParamI<uiParamGroup.params.length; uiParamGroupParamI++) {
						var uiParamGroupParam = uiParamGroup.params[uiParamGroupParamI];
						params[uiParamGroup.name].push({
							"name": uiParamGroupParam.name,
							"params": uiParamGroupParam.getValue()
						});
					}
				}
				params.extra = {
					"enable_scatter": $("#cb-run-enable-scatter").is(":checked"),
					"enable_tsne": $("#cb-run-enable-tsne").is(":checked"),
				}

				// start run
				this._exec_start();
				$.ajax({
					url: "/execStart",
					type: "POST",
					data: JSON.stringify(params),
					contentType: "application/json",
					dataType: "json",
				}).done(function(json) {
					if (json.status == "ok") {
						self.runId = json.runId;
						self.execUpdate();
					} else {
						self.notifyCommunicationError(json.errorMessage);
					}
				}).fail(function() {
					self.notifyCommunicationError("unexpected error occurred");
					self._exec_end();
				});
			},
			execUpdate: function() {
				var self = this;

				setTimeout(function() {
					$.ajax({
						url: "/execUpdate",
						data: {
							runId: self.runId
						},
						dataType: "json",
					}).done(function(json) {
						if (json.status == "ok") {
							// add text update (and scroll if necessary)
							var outputEle = $(self).find(".view-result .output");
							var newText = outputEle.val() + json.update;

							var scrollPcntg = outputEle.get(0).scrollTop / (outputEle.get(0).scrollHeight - outputEle.get(0).clientHeight);
							var scroll = !isNaN(scrollPcntg) && scrollPcntg<1 ? outputEle.scrollTop() : null;
							outputEle.val(newText);
							outputEle.scrollTop(scroll!=null ? scroll : outputEle.get(0).scrollHeight);

							// continue
							if (json.execStatus == "running") {
								// still running
								self.execUpdate();								
							} else {
								// finished
								if (json.execStatus == "error") {
									// error
									self.notifyError(json.execError);
								} else {
									// success
									self.runResult = json;
									self._displayResult(json);
								}
								self._exec_end();
							}
						} else {
							self.notifyCommunicationError(json.errorMessage);
							self._exec_end();
						}
					}).fail(function() {
						self.notifyCommunicationError("unexpected error occurred");
						self._exec_end();
					});
				}, CONF_EXEC_UPDATE_INTERVAL);
			},
			_exec_start: function() {
				this.runId = null;
				this.runResult = null;

				this._exec_disableButton();
				$(this).find(".view-result .output").val("");
				$(this).find(".view-result .view-result-events-container").empty();
				$(this).find(".bt-plot").button("disable");
				$(this).find(".bt-csv").button("disable");

				$(this).find(".view-result").css("background-color", "#FFFFFF");
			},
			_exec_end: function() {
				this.runId = null;

				this._exec_enableButton();
			},
			_exec_disableButton: function() {
				$("#cb-run-enable-scatter").attr("disabled", "disabled");
				$("#cb-run-enable-tsne").attr("disabled", "disabled");
				$(this).find("button.bt-run").button("disable");
				$(this).find("img.bt-run-load").show();
			},
			_exec_enableButton: function() {
				$(this).find("img.bt-run-load").hide();
				$(this).find("button.bt-run").button("enable");
				$("#cb-run-enable-scatter").removeAttr("disabled");
				$("#cb-run-enable-tsne").removeAttr("disabled");
			},
			_displayResult: function(result) {
				// add events
				for (var eventI=0; eventI<result.events.length; eventI++) {
					var event = result.events[eventI];
					$(this).find(".view-result-events-container").append(resultEventTpl.render(event));
				}
				// set up csv download
				$(this).find(".bt-csv").attr("href", "data:text/octet-stream;base64,"+base64.encode(result.csv));
				$(this).find(".bt-plot").button("enable");
				$(this).find(".bt-csv").button("enable");
			},
			_plotIsOpen: false,
			_openPlot: function(dialogTpl) {
				var self = this;
				if (this.runResult != null) {
					if (self._plotIsOpen)
						return;
					/*
					$.ajax({
						url: "/data",
						dataType: "json",
					}).done(function(json) {
						var dialogData = {
							"title": self.view.tabTitle,
							"events": [],
							"data": {
								"data": json.data,
								"ranges": json.ranges,
								"axisScales": json.axisScales,
								"labelCount": json.labelCount,
								"dataCount": 5,
							},
							"tsne": null
						}

						var dialog = dialogTpl.render(dialogData);
						$(dialog).on("dialogClose", function(event, ui) {
							$(dialog).remove();
						})
						$("body").append(dialog);
					});*/
			
					var dialogData = {
						"title": this.view.tabTitle,
						"events": this.runResult.events,
						"data": self.runResult.data != null ? {
							"data": self.runResult.data.data,
							"ranges": self.runResult.data.ranges,
							"axisScales": self.runResult.data.axisScales,
							"labelCount": self.runResult.data.labelCount,
						} : null,
						"tsne": this.runResult.tsne,
						"dataCount": self.runResult.dataCount
					}

					var dialog = dialogTpl.render(dialogData);
					self._plotIsOpen = true;
					$(dialog).on("dialogClose", function(event, ui) {
						$(dialog).remove();
						self._plotIsOpen = false;
					})
					$("body").append(dialog);
				}
			},

			notifyCommunicationError: function(err) {
				alert(err);
			},
			notifyError: function(err) {
				alert(err);
			},
		}
	);

});