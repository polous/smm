import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventFilter

class ContributorNumberThreshold(EventFilter):
	def __init__(self, contributorNumberThreshold):
		self.threshold = contributorNumberThreshold

	def filter(self, event):
		user_count = 0
		users = {}
		for photo in event.ContentCluster:
			if not(photo.Publisher in users):
				users[photo.Publisher] = True
				user_count += 1

		filter = user_count < self.threshold
		if filter:
			print "ContributorNumberThreshold: ", user_count, " < ", self.threshold
		return filter