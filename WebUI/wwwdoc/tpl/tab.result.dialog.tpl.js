define(["text!tpl/tab.result.dialog.tpl.html", "tpl/tab.result.dialog.legendentry.tpl.js"], function(tpl, legendEntryTpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;

			// init dialog
			var dialog = $(this).find(".dialog").dialog({
				title: view.title,
				autoOpen: false,
				width: "auto",
				height: "auto",
				close: function(event, ui) {
					dialog.find(".SCATTER_PLOT").children().detach();
					dialog.dialog("destroy");
					$(self).trigger("dialogClose");
				}
			});

			// popuplate legend
			dialog.find(".legend-container").empty();
			dialog.find(".legend-container").append(legendEntryTpl.render({
				label: "no cluster",
				color: ScatterPlot.getColorForLabel(i+1, view.events.length),
				event: null
			}));
			for (var i=0; i<view.events.length; i++) {
				dialog.find(".legend-container").append(
					legendEntryTpl.render({
						label: "Event Cluster: x" + view.events[i].cluster_size,
						color: ScatterPlot.getColorForLabel(i+1, view.events.length),
						event: view.events[i]
					})
				);
			}

			// show tsne plot
			dialog.find(".tsne").attr("src", "data:image/png;base64,"+view.tsne)

			// plot
			if (view.data != null) {
				var plot = ScatterPlot(view.data.data, view.data.ranges, view.data.axisScales, view.data.labelCount, ["latitude", "time", "longitude"]);
				dialog.find(".SCATTER_PLOT").append(plot);
			}

			// attach to "select image" selection box
			dialog.find(".img-select").change(function(eve) {
				if (dialog.find(".img-select").val() == "scatter") {
					dialog.find(".TSNE_IMG").hide();
					dialog.find(".SCATTER_PLOT").show();
				} else {
					dialog.find(".SCATTER_PLOT").hide();
					dialog.find(".TSNE_IMG").show();
				}
			});

			// open dialog
			dialog.dialog("open");
		}, {

		},
		function(view) {
			var alteredView = {
				"data_count": view.dataCount,
				"num_clusters": view.events.length
			};
			return alteredView;
		}
	);

});