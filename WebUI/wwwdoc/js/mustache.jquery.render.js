/**
 * Clones the current jQuery element and renders it with Mustache.render.
 * 
 * @param  {object} view        		(optional) [see mustache.js]
 * @param  {object} partials    		(optional) [see mustache.js]
 * @param  {function} controller		(optional) gets called with "this" pointing to the rendered DOM element with "view" and "partials" as arguments
 * @param  {object} prototype			(optional) prototype object the resulting DOM element gets extended with
 * @param  {function} viewConverter 	(optional) alters the passed "view" before passing it along to Mustache.render, the controller still receives the original value
 * @param  {function} partialsConverter	(optional) alters the passed "partials" before passing it along to Mustache.render, the controller still receives the original value
 * @return {jQuery element} 			the rendered template
 */
$.fn.render = function(view, partials, controller, prototype, viewConverter, partialsConverter) {
	var alteredView = viewConverter!=null ? viewConverter(view) : view;
	var alteredPartials = partialsConverter!=null ? partialsConverter(partials) : partials;
	var $ele = $($.parseHTML(Mustache.render($("<div/>").append(this.clone()).html(), alteredView, alteredPartials)));
	var ele = $ele.get(0);

	if (prototype != null) {
		$.extend(true, ele, prototype);
	}
	if (controller != null) {
		controller.apply(ele, [view, partials]);
	}
	return $ele;
}