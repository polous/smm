define(["text!tpl/params/int.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			$(this).find(".uiparam-param-int .spinner").spinner({
				incremental: true
			});
		}, {
			getValue: function() {
				return $(this).find(".uiparam-param-int .spinner").spinner("value");
			}
		}
	);

});