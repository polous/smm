from FrameworkModels import BasicContent, SocialMediaContentProvider
from inc import helper, algos

from instagram.client import InstagramAPI
import sqlite3
from dateutil.relativedelta import relativedelta


API_KEY = "9480ee57736a409faec27bb672e6db2a"
API_SECRET = "d1d2f900d09e48779b2c01e6d156b5d2"



class InstagramMedia(BasicContent):
	def __init__(self, dbmedia):
		super(InstagramMedia, self).__init__(dbmedia['id'], dbmedia['caption'], dbmedia['timestamp'], helper.GeoLocation(dbmedia['geo_latitude'], dbmedia['geo_longitude']), dbmedia['owner'], dbmedia['url'])

class InstagramContentProvider(SocialMediaContentProvider):
	def __init__(self, dt_range, geo_bb, dbfile="../Toolkit/SocialMediaContentProviders/Instagram.sqlite"):
		#if dt_range.end-dt_range.start > datetime.timedelta(days=7): # CIRCUMVENTED by chunk downloading
		#	raise Exception("Instagram only supports a timespan of 7 days max.")

		geo_lat_center = geo_bb.latitude[0] + (geo_bb.latitude[1]-geo_bb.latitude[0])/2.
		geo_lon_center = geo_bb.longitude[0] + (geo_bb.longitude[1]-geo_bb.longitude[0])/2.
		geo_radius = max(algos.latlon_distance((geo_bb.latitude[0], geo_lon_center), (geo_bb.latitude[1], geo_lon_center)), algos.latlon_distance((geo_lat_center, geo_bb.longitude[0]), (geo_lat_center, geo_bb.longitude[1])))
		if geo_radius > 5:
			raise Exception("Instagram only supports querying a 5km radius, " + str(geo_radius) + " given.")

		super(InstagramContentProvider, self).__init__(dt_range, geo_bb)

		self._geo_lat_center = geo_lat_center
		self._geo_lon_center = geo_lon_center
		self._geo_radius = geo_radius

		self.dbcon = sqlite3.connect(dbfile)
		self.dbcon.row_factory = sqlite3.Row


	def GetContent(self):
		data = []
		
		self._load_data()

		dbres = self.dbcon.execute("\
			SELECT\
				id, owner, caption, timestamp, geo_latitude, geo_longitude, url\
			FROM\
				instagram_media\
			WHERE\
				timestamp>=? AND timestamp<=? AND geo_latitude>=? AND geo_latitude<=? AND geo_longitude>=? AND geo_longitude<=?\
			",

			(helper.datetime_to_ts(self.DateTimeRange.start), helper.datetime_to_ts(self.DateTimeRange.end), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
		)

		for row in dbres:
			data.append(InstagramMedia(row))

		return data

	def _load_data(self):
		api = InstagramAPI(client_id=API_KEY, client_secret=API_SECRET)

		chunks = (self.DateTimeRange.end-self.DateTimeRange.start).days+1
		for i in range(chunks):
			# assemble arguments
			arg_timestart = self.DateTimeRange.start + relativedelta(days=+i)
			arg_timeend   = arg_timestart + relativedelta(days=+1) + relativedelta(seconds=-1)
			print "\tprocessing.. " + arg_timestart.strftime("%Y-%m-%d")

			# check if chunk already loaded
			dbres = self.dbcon.execute("\
				SELECT\
					COUNT(*) AS `count`\
				FROM\
					instagram_media_index\
				WHERE\
					timestamp_low IS NOT NULL AND timestamp_low<=?\
					AND timestamp_high IS NOT NULL AND timestamp_high>=?\
					AND geo_latitude_low<=?\
					AND geo_latitude_high>=?\
					AND geo_longitude_low<=?\
					AND geo_longitude_high>=?\
				",

				(helper.datetime_to_ts(arg_timestart), helper.datetime_to_ts(arg_timeend), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
			).fetchone()

			if dbres is None or dbres["count"] > 0:
				print "\t\tCACHED"
				continue

			# download media
			results = api.media_search(lat=self._geo_lat_center, lng=self._geo_lon_center, distance=self._geo_radius*1000, min_timestamp=helper.datetime_to_ts(arg_timestart), max_timestamp=helper.datetime_to_ts(arg_timeend))

			for media in results:
				mid = media.id
				owner = int(media.user.id)
				caption = "" if media.caption is None else media.caption.text
				timestamp = helper.datetime_to_ts(media.created_time)
				geo_lat = float(media.location.point.latitude)
				geo_lon = float(media.location.point.longitude)
				url = media.link
			    
				try:
					print "\t\t---> INSERT: ", mid, caption
					self.dbcon.execute('DELETE FROM instagram_media WHERE id=?', (mid,))
					self.dbcon.execute('INSERT INTO instagram_media(id, owner, caption, timestamp, geo_latitude, geo_longitude, url) VALUES(?,?,?,?,?,?,?)', (mid, owner, caption, timestamp, geo_lat, geo_lon, url))
				except:
					import traceback
					print "insert failed:", traceback.format_exc()
					pass
			self.dbcon.commit()

			# add to index
			# TODO: once a day is added to the index it won't get queried again => if the current day is added no future entries for this day will be recorded
			self.dbcon.execute("\
				INSERT INTO\
					instagram_media_index(timestamp_low, timestamp_high, geo_latitude_low, geo_latitude_high, geo_longitude_low, geo_longitude_high)\
				VALUES\
					(?,?,?,?,?,?)\
				",

				(helper.datetime_to_ts(arg_timestart), helper.datetime_to_ts(arg_timeend), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
			)
			self.dbcon.commit()