#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
import ui_constructors
import datetime
from dateutil.relativedelta import relativedelta

UIParameterOptions = {
	"dataProviders": {
		"label": "Data Providers",
		"sortIndex": 0,
		"options": {
			"flickr": {
				"label": "Flickr",
				"sortIndex": 0,
				"params": {
					"flickrTimeAttr": {
						"label": "Flickr Time Attribute",
						"sortIndex": 0,
						"template": "tpl/params/select.tpl.js",
						"view": {"options": ["datetime_taken", "datetime_posted"], "default": "datetime_taken"},
					},
					"dt_start": {
						"label": "Datetime from",
						"sortIndex": 1,
						"template": "tpl/params/datetimepicker.tpl.js", 
						"view": {"default": "2012/09/01 00:00"}, 
						"constructor": ui_constructors.parse_datetime,
					},
					"dt_end": {
						"label": "Datetime to",
						"sortIndex": 2,
						"template": "tpl/params/datetimepicker.tpl.js",
						"view": {"default": "2012/10/31 23:59"},
						"constructor": ui_constructors.parse_datetime,
					},
					"geo_bb": {
						"label": "Geo Bounding Box",
						"sortIndex": 3,
						"template": "tpl/params/geoboundingbox.tpl.js",
						#"view": {"latitude": (48.0417, 48.2292), "longitude": (11.3771, 11.7490)},
						"view": {"latitude": (48.1221, 48.1579), "longitude": (11.5363, 11.6033)},
						"constructor": ui_constructors.parse_geobb,
					}
				},
				"constructor": ui_constructors.UIParameterOption_DataProviders_Flickr
			},
			"instagram": {
				"label": "Instagram",
				"sortIndex": 1,
				"params": {
					"dt_start": {
						"label": "Datetime from",
						"sortIndex": 1,
						"template": "tpl/params/datetimepicker.tpl.js", 
						"view": {"default": "2013/09/01 00:00"}, 
						"constructor": ui_constructors.parse_datetime,
					},
					"dt_end": {
						"label": "Datetime to",
						"sortIndex": 2,
						"template": "tpl/params/datetimepicker.tpl.js",
						"view": {"default": "2013/10/31 23:59"},
						"constructor": ui_constructors.parse_datetime,
					},
					"geo_bb": {
						"label": "Geo Bounding Box",
						"sortIndex": 3,
						"template": "tpl/params/geoboundingbox.tpl.js",
						"view": {"latitude": (48.1221, 48.1579), "longitude": (11.5363, 11.6033)},
						"constructor": ui_constructors.parse_geobb,
					}
				},
				"constructor": ui_constructors.UIParameterOption_DataProviders_Instagram
			},
			"twitter": {
				"label": "Twitter",
				"sortIndex": 2,
				"params": {
					"query": {
						"label": "Query (mandatory)",
						"sortIndex": 0,
						"template": "tpl/params/string.tpl.js",
						"view": {"default": "munich"}
					},
					"dt_start": {
						"label": "Datetime from",
						"sortIndex": 1,
						"template": "tpl/params/datetimepicker.tpl.js", 
						"view": {"default": (datetime.datetime.now()-relativedelta(days=6)).strftime("%Y/%m/%d 00:00")}, 
						"constructor": ui_constructors.parse_datetime,
					},
					"dt_end": {
						"label": "Datetime to",
						"sortIndex": 2,
						"template": "tpl/params/datetimepicker.tpl.js",
						"view": {"default": datetime.datetime.now().strftime("%Y/%m/%d %H:%M")},
						"constructor": ui_constructors.parse_datetime,
					},
					"geo_bb": {
						"label": "Geo Bounding Box",
						"sortIndex": 3,
						"template": "tpl/params/geoboundingbox.tpl.js",
						"view": {"latitude": (48.0417, 48.2292), "longitude": (11.3771, 11.7490)},
						"constructor": ui_constructors.parse_geobb,
					}
				},
				"constructor": ui_constructors.UIParameterOption_DataProviders_Twitter
			}
		}
	},
	"dataFilters": {
		"label": "Data Filters",
		"sortIndex": 1,
		"options": {
			"removeSimilarsFromSameUser": {
				"label": "Equalize User Contribution",
				"params": {
					"timespan": {
						"label": "Timespan (in seconds)",
						"sortIndex": 0,
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 60*60*2},
					},
					"levenshtein_ratio": {
						"label": "Post Similarity (Levenshtein ratio)",
						"sortIndex": 1,
						"template": "tpl/params/floatslider.tpl.js",
						"view": {"range": (0,1), "step": 0.001, "default": 0.5},
					}
				},
				"constructor": ui_constructors.UIParameterOption_DataFilters_RemoveSimilarsFromSameUser
			}
		}
	},
	"eventDetectionAlgorithms": {
		"label": "Event Detection Algorithms",
		"sortIndex": 2,
		"options": {
			"textualDBSCAN": {
				"label": "Text Featured DBSCAN",
				"sortIndex": 0,
				"params": {
					"dt_scale": {
						"label": "datetime scale",
						"sortIndex": 0,
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 60*60*24*4}
					},
					"geo_lat_scale": {
						"label": "geo latitude scale",
						"sortIndex": 1,
						"template": "tpl/params/float.tpl.js",
						"view": {"default": 0.0075, "step": 0.0001}
					},
					"geo_long_scale": {
						"label": "geo longitude scale",
						"sortIndex": 2,
						"template": "tpl/params/float.tpl.js",
						"view": {"default": 0.0075, "step": 0.0001}
					},
					"text_dimensions": {
						"label": "text: number of words",
						"sortIndex": 3,
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 100}
					},
					"text_filter": {
						"label": "text: ignore words",
						"sortIndex": 4,
						"template": "tpl/params/stringlist.tpl.js",
						"view": {
							"default": ["a", "the", "and", "or", "in", "of", "with",
								"munich", "munchen", "muenchen", u"münchen",
								"der", "die", "das", "und",
								"2012",]
						}
					},
					"text_filterMethods": {
						"label": "text: filter methods",
						"sortIndex": 5,
						"template": "tpl/params/options.tpl.js",
						"view": {
							"optionGroup": "textFilters",
							"label": "Text Filters",
							"default": ["numericFilter", "minLengthFilter"]
						},
						"constructor": ui_constructors.OptionsConstructor
					},
					"text_scale": {
						"label": "text scale",
						"sortIndex": 6,
						"template": "tpl/params/float.tpl.js",
						"view": {"default": 0.5, "step": 0.1}
					},
					"text_penalty": {
						"label": "text penalty (for photos that have no single word in common)",
						"sortIndex": 7,
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 999999}
					},
					"eps": {
						"label": "eps (epsilon neighbourhood)",
						"sortIndex": 8,
						"template": "tpl/params/float.tpl.js",
						"view": {"default": 1, "step": 0.01}
					},
					"min_samples": {
						"label": "min. number of samples",
						"sortIndex": 9,
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 7},
					}
				},
				"constructor": ui_constructors.UIParameterOption_EventDetectionAlgorithms_TextualDBSCAN
			},
		}
	},
	"clusterFilters": {
		"label": "Event Cluster Filters",
		"sortIndex": 3,
		"options": {
			"removeUnrelatedPhotos": {
				"label": "Remove unrelated Content",
				"constructor": ui_constructors.UIParameterOption_ClusterFilters_RemoveUnrelatedPhotos
			}
		}
	},
	"eventInfo": {
		"label": "Event Construction",
		"sortIndex": 4,
		"options": {
			"name": {
				"label": "Name",
				"sortIndex": 0,
				"params": {
					"importance": {
						"label": "Single-Word-Importance (compare word usage in cluster vs its usage in all data; 0=use all .. 1=ignore all)",
						"sortIndex": 0,
						"template": "tpl/params/floatslider.tpl.js",
						"view": {"range": (0,1), "step": 0.01, "default": 0.3},
					},
					"falloff": {
						"label": "Fall-Off (0=include all important words; 1=include only the most important word)",
						"sortIndex": 0,
						"template": "tpl/params/floatslider.tpl.js",
						"view": {"range": (0,1), "step": 0.01, "default": 0.3},
					}
				},
				"constructor": ui_constructors.UIParameterOption_EventInfo_Name
			},
			"time": {
				"label": "Time",
				"sortIndex": 1,
				"constructor": ui_constructors.UIParameterOption_EventInfo_Time
			},
			"location": {
				"label": "Location",
				"sortIndex": 2,
				"constructor": ui_constructors.UIParameterOption_EventInfo_Location
			},
			"osm_link": {
				"label": "Open Street Map Link",
				"sortIndex": 3,
				"constructor": ui_constructors.UIParameterOption_EventInfo_OSMLink
			},
			"wiki_link": {
				"label": "Wikipedia Link",
				"sortIndex": 4,
				"constructor": ui_constructors.UIParameterOption_EventInfo_WikiLink
			},
			"contributorNumber": {
				"label": "Number of Contributors",
				"sortIndex": 5,
				"constructor": ui_constructors.UIParameterOption_EventInfo_ContributorNumber
			},
			"contentPreview": {
				"label": "Content Preview",
				"sortIndex": 6,
				"constructor": ui_constructors.UIParameterOption_EventInfo_ContentPreview
			},
			"related_words": {
				"label": "Related Words",
				"sortIndex": 7,
				"params": {
					"importance": {
						"label": "Single-Word-Importance (compare word usage in cluster vs its usage in all data; 0=use all .. 1=ignore all)",
						"sortIndex": 0,
						"template": "tpl/params/floatslider.tpl.js",
						"view": {"range": (0,1), "step": 0.01, "default": 0.1},
					},
					"falloff": {
						"label": "Fall-Off (0=include all important words; 1=include only the most important word)",
						"sortIndex": 0,
						"template": "tpl/params/floatslider.tpl.js",
						"view": {"range": (0,1), "step": 0.01, "default": 0.1},
					}
				},
				"constructor": ui_constructors.UIParameterOption_EventInfo_RelatedWords
			},
			"""
			"related_places": {
				"label": "Related Places",
				"sortIndex": 8,
				"constructor": ui_constructors.UIParameterOption_EventInfo_RelatedPlaces
			},
			"""
			"occurrencePattern": {
				"label": "Occurence Pattern",
				"sortIndex": 9,
				"constructor": ui_constructors.UIParameterOption_EventInfo_OccurrencePattern
			}
		}
	},
	"eventFilters": {
		"label": "Event Filters",
		"sortIndex": 5,
		"options": {
			"contributorNumberThreshold": {
				"label": "Contributor Number Threshold",
				"sortIndex": 0,
				"params": {
					"threshold": {
						"label": "Minimum Number of Contributors",
						"sortIndex": 0,
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 3},
					}
				},
				"constructor": ui_constructors.UIParameterOption_EventFilters_ContributorNumberThreshold
			},
			"contentClusterSizeThreshold": {
				"label": "Content-Cluster Size Threshold",
				"sortIndex": 1,
				"params": {
					"threshold": {
						"label": "Minimum Number of Content Entities",
						"sortIndex": 0,
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 7},
					}
				},
				"constructor": ui_constructors.UIParameterOption_EventFilters_ContentClusterSizeThreshold
			},
			"gaussianShapeFilter": {
				"label": "Gaussian Shape Filter",
				"sortIndex": 2,
				"params": {
					"match_ratio": {
						"label": "Match Ratio",
						"sortIndex": 0,
						"template": "tpl/params/floatslider.tpl.js",
						"view": {"range": (0,1), "step": 0.001, "default": 0.845},
					}
				},
				"constructor": ui_constructors.UIParameterOption_EventFilters_GaussianShapeFilter
			}
		}
	},
	"textFilters": {
		"label": "(additional) Text Filters",
		"sortIndex": 100,
		"options": {
			"numericFilter": {
				"label": "Filter plain Numbers",
				"constructor": ui_constructors.UIParameterOption_TextFilters_NumericFilter
			},
			"minLengthFilter": {
				"label": "Filter short Words",
				"params": {
					"minLength": {
						"label": "minimum length",
						"template": "tpl/params/int.tpl.js",
						"view": {"default": 3}
					}
				},
				"constructor": ui_constructors.UIParameterOption_TextFilters_MinLengthFilter
			}
		}
	}
}

UIParameters = {
	"dataProvider": {
		"label": "1. Data Acquisition: Data Provider",
		"sortIndex": 0,
		"optionGroup": "dataProviders",
		"optionCountMin": 1,
		"optionCountMax": 1,

		"default": ["flickr"]
	},
	"dataFilters": {
		"label": "(optional) Data Filters",
		"label-css": "padding-left: 15px;",
		"sortIndex": 1,
		"optionGroup": "dataFilters",

		"default": ["removeSimilarsFromSameUser"]
	},
	"eventDetectionAlgorithm": {
		"label": "2. Event Detection Algorithm",
		"sortIndex": 2,
		"optionGroup": "eventDetectionAlgorithms",
		"optionCountMin": 1,
		"optionCountMax": 1,

		"default": ["textualDBSCAN"]
	},
	"clusterFilters": {
		"label": "(optional) Event Cluster Filters",
		"label-css": "padding-left: 15px;",
		"sortIndex": 3,
		"optionGroup": "clusterFilters",

		"default": ["removeUnrelatedPhotos"]
	},
	"eventInfo": {
		"label": "3. Event Construction",
		"sortIndex": 4,
		"optionGroup": "eventInfo",

		"default": ["name", "time", "location", "related_words", "contentPreview", "osm_link", "wiki_link"]
	},
	"eventFilters": {
		"label": "(optional) Event Filters",
		"label-css": "padding-left: 15px;",
		"sortIndex": 5,
		"optionGroup": "eventFilters",

		"default": ["contributorNumberThreshold", "contentClusterSizeThreshold", "gaussianShapeFilter"]
	}
}

UIEventInfo = {
	"osm_link": "tpl/event_info/osm_link.tpl.js",
	"wiki_link": "tpl/event_info/wiki_link.tpl.js",
	"contentPreview": "tpl/event_info/contentPreview.tpl.js"
}