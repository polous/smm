define(["text!tpl/tab.result.dialog.legendentry.tpl.html", "tpl/tab.result.event.tpl.js"], function(tpl, resultEventTpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;

			if (view.event != null)
				$(this).find(".event").append(resultEventTpl.render(view.event));
		}, {
		}
	);

});