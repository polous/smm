def get_averageWordUsage(data):
	from inc import helper

	avg_word_usage = {}

	dataCount = len(data)

	for datum in data:
		words = []

		unfilteredWords = datum.Title.split(" ")
		for word in unfilteredWords:
			fword = helper.filter_word(word)
			if not(fword is None):
				words.append(fword)

		for word in words:
			if not(word in avg_word_usage):
				avg_word_usage[word] = 0
			avg_word_usage[word] += 1. / dataCount

	return avg_word_usage

def get_mostFrequentlyUsedWords(data, count, word_filter=None):
	from inc import word_filters
	import operator

	word_filter_map = {}
	word_filter_objs = []
	if not(word_filter is None):
		for word in word_filter:
			if isinstance(word, word_filters.WordFilter):
				word_filter_objs.append(word)
			else:
				word_filter_map[word] = True

	mostFreqUsedWords = {}
	mostFreqUsedWordsCounter = 0

	averageWordUsage = get_averageWordUsage(data)

	sorted_words = sorted(averageWordUsage.iteritems(), key=operator.itemgetter(1), reverse=True)

	for sorted_word in sorted_words:
		if sorted_word[0] in word_filter_map:
			continue
		skip = False
		for f in word_filter_objs:
			if f.filter(sorted_word[0]):
				skip=True
				break
		if skip:
			continue

		mostFreqUsedWords[sorted_word[0]] = sorted_word[1]
		mostFreqUsedWordsCounter += 1
		if mostFreqUsedWordsCounter >= count:
			break

	return mostFreqUsedWords


def extract_event_gaussians(data, weights=None, origDimensions=3):
	import numpy as np

	augmentedData = None
	
	if weights is None:
		augmentedData = data
	else:
		augmentedData = []
		for datum in data:
			weight = 0

			for weightCandidate in weights:
				found = True
				for i in range(origDimensions):
					if weightCandidate[i] != datum[i]:
						found = False
						break
				if found:
					weight = np.sum(datum[origDimensions:])
					break

			for i in range(weight):
				augmentedData.append(datum[0:origDimensions])
		augmentedData = np.asarray(augmentedData)

	return fit_gaussian(augmentedData)

	
"""
	fits a gaussian to every dimension of the given data

	Params:
		data 	np.array containing all data points

	Returns:
		means, stds 	(arrays containing the mean and the std of each gaussian)
"""
def fit_gaussian(data):
	import numpy as np
	
	means = []
	stds = []

	for idim in range(data.shape[1]):
		ddata = data[:,idim]
		
		"""
		mean= ddata.mean()
		std = ddata.std()
		"""

		"""
		import scipy.stats 
		mean,std= scipy.stats.norm.fit(ddata)
		"""
		
		# redefine mean as the median of the data
		mean = np.median(ddata)#np.mean(ddata)

		# calculate standard deviation from the "new mean" (the median)
		std = np.sqrt((ddata-mean).transpose().dot(ddata - mean) / ddata.shape[0])

		"""
		# ALTERNATIVE: calculate standard deviation in a "gaussian interpretation". the standard deviation of a gaussian covers 68,27..% of the mass of the data
		std = 0
		possible_stds = np.sort(np.abs(ddata-mean))
		for possible_std in possible_stds:
			std = possible_std

			stdMass = ddata[(ddata>=(mean-std)) & (ddata<=(mean+std))].shape[0]
			if stdMass >= ddata.shape[0]*0.6827:
				break
		"""

		means.append(mean)
		stds.append(std)

	return means, stds

def latlon_distance(origin, destination):
	"""
		Parameters:
			origin 			(latitude, longitude)
			destination		(latitude, longitude)
	"""
	# Haversine formula example in Python
	# Author: Wayne Dyck (http://www.platoscave.net/blog/2009/oct/5/calculate-distance-latitude-longitude-python/)

	import math
	lat1, lon1 = origin
	lat2, lon2 = destination
	radius = 6371 # km

	dlat = math.radians(lat2-lat1)
	dlon = math.radians(lon2-lon1)
	a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
		* math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
	d = radius * c

	return d