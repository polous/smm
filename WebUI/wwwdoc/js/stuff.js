function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function iterateObjectSorted(obj, worker, sortfunction) {
    var keys = [];
    for (var key in obj)
    {
        if (obj.hasOwnProperty(key))
            keys.push(key);
    }
    if (sortfunction != null)
        keys.sort(sortfunction);
    else
        keys.sort();

    for (var i = 0; i < keys.length; i++)
    {
        worker(keys[i], obj[keys[i]]);
    }
}