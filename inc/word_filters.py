"""
	Filters words.
"""
class WordFilter(object):
	"""
		Params:
			word:		word in question

		Returns:
			TRUE to filter word || FALSE to keep it
	"""
	def filter(self, word):
		raise "not implemented"

class NumericFilter(WordFilter):
	def filter(self, word):
		try:
			float(word)
			return True
		except:
			return False

class MinLengthFilter(WordFilter):
	def __init__(self, len):
		self.len = len

	def filter(self, word):
		return len(word) < self.len