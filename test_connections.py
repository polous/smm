import datetime
from inc import helper

###
### CONFIG
###
DT_END   = datetime.datetime.now()
DT_START = DT_END - datetime.timedelta(days=1)
GEO_BB   = helper.GeoBoundingBox(48.1221, 11.5363, 48.1579, 11.6033) # Munich


###
### FLICKR
###
FLICKR_TIMEATTR = "datetime_taken"
FLICKR_DATABASE = "Toolkit/SocialMediaContentProviders/Flickr.sqlite"

print "testing FlickrContentProvider with: ", (DT_START, DT_END, GEO_BB, FLICKR_TIMEATTR)

from Toolkit.SocialMediaContentProviders.Flickr import FlickrContentProvider
flickr = FlickrContentProvider(helper.DateTimeRange(DT_START, DT_END), GEO_BB, flickr_timeattr=FLICKR_TIMEATTR, dbfile=FLICKR_DATABASE)
flickrContent = flickr.GetContent()

print ".. SUCCESS !"