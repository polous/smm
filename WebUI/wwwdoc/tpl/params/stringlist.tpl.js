define(["text!tpl/params/stringlist.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;

			if (view.default != null)
				for (var i=0; i<view.default.length; i++)
					this.addString(view.default[i]);

			$(this).find(".param-stringlist-input-bt").click(function() {
				self.submitString();
			});
			$(this).find(".param-stringlist-input").keyup(function(eve) {
				if (eve.keyCode == 13) {
					self.submitString();
				}
			});
		}, {
			submitString: function() {
				this.addString($(this).find(".param-stringlist-input").val());
				$(this).find(".param-stringlist-input").val("");
			},

			addString: function(str) {
				var ele = $("<div/>").addClass("param-stringlist-entry").css("position", "relative").text(str);
				var eleDelBt = $("<button/>").css("position", "absolute").css("top", 0).css("right", 0);
				eleDelBt.button({				
					icons: {
						primary: "ui-icon-close"
					},
					text: false
				}).click(function() {
					$(this).parents(".param-stringlist-entry").remove();
				});
				ele.append(eleDelBt);

				$(this).find(".param-stringlist").append(ele);
			},

			getValue: function() {
				var list=[];
				$(this).find(".param-stringlist div").each(function() {
					list.push($(this).text());
				});
				return list;
			}
		}
	);

});