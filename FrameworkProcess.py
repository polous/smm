import numpy as np
import datetime

import FrameworkModels

def DetectEvents(DATA_PROVIDER, DATA_FILTERS, EVENT_DETECTION_ALGORITHM, CLUSTER_FILTERS, EVENT_INFO, EVENT_FILTERS):

	##
	## verify input
	##
	if not(isinstance(DATA_PROVIDER, FrameworkModels.SocialMediaContentProvider)):
		raise Exception("DATA_PROVIDER must be of type FrameworkModels.SocialMediaContentProvider")
	for f in DATA_FILTERS:
		if not(isinstance(f, FrameworkModels.DataFilter)):
			raise Exception("DATA_FILTERS must be of type List<FrameworkModels.DataFilter>")
	if not(isinstance(EVENT_DETECTION_ALGORITHM, FrameworkModels.EventDetectionAlgorithm)):
		raise Exception("DATA_PROVIDER must be of type FrameworkModels.EventDetectionAlgorithm")
	for f in CLUSTER_FILTERS:
		if not(isinstance(f, FrameworkModels.ClusterFilter)):
			raise Exception("DATA_FILTERS must be of type List<FrameworkModels.ClusterFilter>")
	for infoExtractor in EVENT_INFO:
		if not(isinstance(infoExtractor, FrameworkModels.EventInfoExtractor)):
			raise Exception("EVENT_INFO must be of type List<FrameworkModels.EventInfoExtractor>")
	for f in EVENT_FILTERS:
		if not(isinstance(f, FrameworkModels.EventFilter)):
			raise Exception("DATA_FILTERS must be of type List<FrameworkModels.EventFilter>")


	###
	### run process
	###
	
	## 1. load raw data
	print "\n\n\n\n###"
	print "###"
	print "### 1. Loading Raw Data"
	print "###"
	print "###\n"
	RAW_DATA = DATA_PROVIDER.GetContent() 
	print "\n#{}".format(len(RAW_DATA)), "content entities loaded"

	for d in RAW_DATA:
		print "\t{}: {} ({})".format(d.Publisher, d.Title.encode("utf-8"), datetime.datetime.fromtimestamp(d.TimeStamp).strftime("%Y-%m-%d %H:%M:%S"))

	# (optionally) filter raw data
	print "\n\n\n\n###"
	print "###"
	print "### (optionally) Filtering Raw Data"
	print "###"
	print "###\n"
	data = RAW_DATA

	filteredData = []
	for datum in data:
		keep = True
		for filter in DATA_FILTERS:
			if filter.filter(datum):
				print "\tfiltered {}: {} ({}) due to '{}'..".format(datum.Publisher, datum.Title.encode("utf-8"), datetime.datetime.fromtimestamp(datum.TimeStamp).strftime("%Y-%m-%d %H:%M:%S"), str(filter))
				keep = False
				break
		if keep:
			filteredData.append(datum)
	data = filteredData

	if len(data) <= 0:
		return [], []


	## 2. detect clusters
	print "\n\n\n\n###"
	print "###"
	print "### 2. Running Event-Detection-Algorithm"
	print "###"
	print "###\n"
	CLUSTERS = EVENT_DETECTION_ALGORITHM.DetectEvents(data, DATA_PROVIDER)
	for c in CLUSTERS:
		if not(isinstance(c, FrameworkModels.ContentCluster)):
			raise Exception("EVENT_DETECTION_ALGORITHM must return a list of FrameworkModels.ContentClusters, but an entry of type {} was found!".format(type(c)))

	print "\tnumber of clusters: {}".format(len(CLUSTERS))
	for c in CLUSTERS:
		print "\t\tcluster: x" + str(len(c))
		for d in c:
			print "\t\t\t{}: {} ({})".format(d.Publisher, d.Title.encode("utf-8"), datetime.datetime.fromtimestamp(d.TimeStamp).strftime("%Y-%m-%d %H:%M:%S"))



	# (optionally) filter clusters
	print "\n\n\n\n###"
	print "###"
	print "### (optionally) Filtering Content Clusters"
	print "###"
	print "###\n"
	filteredCLUSTERS = []
	for c in CLUSTERS:
		cluster = c

		skip = False
		for filter in CLUSTER_FILTERS:
			filterRes = filter.filter(cluster)
			if filterRes is None:
				print "\t.. cluster (x" + len(cluster) + ") skipped due to '" + str(filter) + "'"
				skip = True
				break
			else:
				cluster = filterRes
		if skip:
			continue
		else:
			filteredCLUSTERS.append(cluster)
	CLUSTERS = filteredCLUSTERS


	## 3. construct events
	print "\n\n\n\n###"
	print "###"
	print "### 3. Constructing Events"
	print "###"
	print "###\n"
	EVENTS = []
	for c in CLUSTERS:
		event_identifier = "UNK"
		event_time = (None, None)
		event_location = {
			"center": (None, None),
			"bbox": {
				"latitude": (None, None),
				"longitude": (None, None)
			}
		}
		event_additionalInfo = {}

		EVENT = FrameworkModels.BasicEvent(event_identifier, event_time, event_location, c, event_additionalInfo)
		for infoExtractor in EVENT_INFO:
			info = infoExtractor.ExtractInfo(RAW_DATA, c, DATA_PROVIDER, EVENT, EVENTS)
			
			if isinstance(infoExtractor, FrameworkModels.EventIdentifierExtractor):
				event_identifier = info
			elif isinstance(infoExtractor, FrameworkModels.EventTimeExtractor):
				event_time = info
			elif isinstance(infoExtractor, FrameworkModels.EventLocationExtractor):
				event_location = info
			else:
				infoKey = str(infoExtractor)
				while infoKey in event_additionalInfo:
					infoKey = infoKey + "_"

				event_additionalInfo[infoKey] = info

			EVENT = FrameworkModels.BasicEvent(event_identifier, event_time, event_location, c, event_additionalInfo)

		EVENTS.append(EVENT)
	
	# (optionally) filter events
	print "\n\n\n\n###"
	print "###"
	print "### (optionally) Filtering Events"
	print "###"
	print "###\n"
	filteredEVENTS = []
	for EVENT in EVENTS:
		skip = False
		for filter in EVENT_FILTERS:
			if filter.filter(EVENT):
				print "\t.. event '" + EVENT.Identifier.encode("utf-8") + "' skipped due to '" + str(filter) + "'"
				skip = True
				break
		if skip:
			continue
		else:
			filteredEVENTS.append(EVENT)
	EVENTS = filteredEVENTS

	return RAW_DATA, EVENTS