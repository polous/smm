import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventInfoExtractor

class UserCount(EventInfoExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		user_count = 0
		users = {}
		for photo in event.ContentCluster:
			if not(photo.Publisher in users):
				users[photo.Publisher] = True
				user_count += 1
		return user_count

	def __str__(self):
		return "UserCount"