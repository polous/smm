define(["text!tpl/params/string.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
		}, {
			getValue: function() {
				return $(this).find("input").val();
			}
		}
	);

});