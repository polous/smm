define(["text!tpl/params/float.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			$(this).find(".uiparam-param-float .spinner").spinner({
				incremental: true,
				step: view.step,
				numberFormat: "n"
			});
		}, {
			getValue: function() {
				return $(this).find(".uiparam-param-float .spinner").spinner("value");
			}
		}
	);

});