define(["text!tpl/params/select.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			if (view.default != null)
				$(this).find("select").val(view.default);
		}, {
			getValue: function() {
				return $(this).find("select").val();
			}
		}
	);

});