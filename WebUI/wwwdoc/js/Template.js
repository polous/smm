/**
 * A renderable template.
 * 
 * @param {string} template 			html template   
 * @param {function} controller 		(optional) gets called once for every rendered template with "this" pointing to the rendered DOM element
 * @param {object} prototype			(optinal) prototype object the rendered DOM element gets extended with
 * @param {function} viewConverter 		(optional) alters the passed "view" before passing it along to Mustache.render, the controller still receives the original value
 * @param {function} partialsConverter	(optional) alters the passed "partials" before passing it along to Mustache.render, the controller still receives the original value
 */
function Template(template, controller, prototype, viewConverter, partialsConverter) {
	if ($(template).length != 1)
		throw "illegal template, must contain exactly one html tag (e.g. a div surrounding the rest of thetemplate)!";

	this.template = template;
	this.controller = controller;
	this.controllerPrototype = prototype;
	this.viewConverter = viewConverter;
	this.partialsConverter = partialsConverter;

	/**
	 * Renders the template with mustache.js
	 * 
	 * @param  {object} view    	[see mustache.js]
	 * @param  {object} partials	[see mustache.js]
	 * @return {DOM element} 		the rendered template
	 */
	this.render = function(view, partials) {
		return $(template).render(view, partials, controller, prototype, viewConverter, partialsConverter).get(0);
	}
}