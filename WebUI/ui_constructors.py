#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

import FrameworkModels
from inc import helper, word_filters
import ui_definition





###
###
### GLOBALS
###
###
def OptionsConstructor(val, alreadyConstructedParams, includeDefinitions=False):
	constructedParam = []
	constructedParamDefinition = []

	uiParamOptions = ui_definition.UIParameterOptions[val["optionGroup"]]["options"]
	for uiParams in val["params"]:
		uiParamOption = uiParamOptions[uiParams["name"]]

		params = {}
		if "params" in uiParamOption:
			for uiParamOption_paramName in uiParamOption["params"]:
				uiParamOption_param = uiParamOption["params"][uiParamOption_paramName]
				uiParams_param = uiParams["params"][uiParamOption_paramName]

				if not("constructor" in uiParamOption_param) or uiParamOption_param["constructor"] is None:
					param = uiParams_param
				else:
					param = uiParamOption_param["constructor"](uiParams_param, alreadyConstructedParams)

				params[uiParamOption_paramName] = param

		constructedParam.append(uiParamOption["constructor"](params, alreadyConstructedParams))
		constructedParamDefinition.append({"name": uiParams["name"], "option": uiParamOption})

	if includeDefinitions:
		return constructedParam, constructedParamDefinition
	else:
		return constructedParam


def parse_datetime(val, alreadyConstructedParams):
	import datetime
	return datetime.datetime.strptime(val, '%Y/%m/%d %H:%M')
def parse_geobb(val, alreadyConstructedParams):
	from inc import helper
	return helper.GeoBoundingBox(float(val["latitude"][0]), float(val["longitude"][0]), float(val["latitude"][1]), float(val["longitude"][1]))
#####################################################################################################################################################




###
###
### SOCIAL MEDIA CONTENT PROVIDERS
###
###
def UIParameterOption_DataProviders_Flickr(params, alreadyConstructedParams):
	from Toolkit.SocialMediaContentProviders.Flickr import FlickrContentProvider

	dt_start = params["dt_start"]
	dt_end = params["dt_end"]
	geo_bb = params["geo_bb"]
	flickr_timeattr = params["flickrTimeAttr"]

	print "instantiating FlickrContentProvider with: ", (dt_start, dt_end, geo_bb, flickr_timeattr)
	return FlickrContentProvider(helper.DateTimeRange(dt_start, dt_end), geo_bb, flickr_timeattr=flickr_timeattr)

def UIParameterOption_DataProviders_Instagram(params, alreadyConstructedParams):
	from Toolkit.SocialMediaContentProviders.Instagram import InstagramContentProvider

	dt_start = params["dt_start"]
	dt_end = params["dt_end"]
	geo_bb = params["geo_bb"]

	print "instantiating InstagramContentProvider with: ", (dt_start, dt_end, geo_bb)
	return InstagramContentProvider(helper.DateTimeRange(dt_start, dt_end), geo_bb)


def UIParameterOption_DataProviders_Twitter(params, alreadyConstructedParams):
	from Toolkit.SocialMediaContentProviders.Twitter import TwitterContentProvider

	dt_start = params["dt_start"]
	dt_end = params["dt_end"]
	geo_bb = params["geo_bb"]
	query = params["query"]

	print "instantiating TwitterContentProvider with: ", (dt_start, dt_end, geo_bb, query)
	return TwitterContentProvider(helper.DateTimeRange(dt_start, dt_end), geo_bb, query=query)



###
###
### EVENT DETECTION ALGORITHMS
###
###
def UIParameterOption_EventDetectionAlgorithms_TextualDBSCAN(params, alreadyConstructedParams):
	from Toolkit.EventDetectionAlgorithms.TextualDBSCAN import TextualDBSCANEventDetectionAlgorithm
	
	dt_scale = params["dt_scale"]
	geo_lat_scale = params["geo_lat_scale"]
	geo_long_scale = params["geo_long_scale"]
	text_dimensions = params["text_dimensions"]
	text_filter = params["text_filter"]
	text_filter += params["text_filterMethods"]
	text_scale = params["text_scale"]
	text_penalty = params["text_penalty"]
	eps = params["eps"]
	min_samples = params["min_samples"]

	print "instantiating TextualDBSCANEventDetectionAlgorithm with: ", (dt_scale, geo_lat_scale, geo_long_scale, text_dimensions, text_filter, text_scale, text_penalty, eps, min_samples)
	return TextualDBSCANEventDetectionAlgorithm(dt_scale, geo_lat_scale, geo_long_scale, text_dimensions, text_filter, text_scale, text_penalty, eps, min_samples)





###
###
### EVENT CONSTRUCTION
###
###
def UIParameterOption_EventInfo_Name(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.Name import Name

	importance = params["importance"]
	falloff = params["falloff"]

	print "instantiating EventInfoExtractors.Name: ", (importance, falloff)
	return Name(importance, falloff)

def UIParameterOption_EventInfo_Time(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.Time import Time

	print "instantiating EventInfoExtractors.Time"
	return Time()

def UIParameterOption_EventInfo_Location(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.Location import Location

	print "instantiating EventInfoExtractors.Location"
	return Location()

def UIParameterOption_EventInfo_OSMLink(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.OpenStreetMapLink import OpenStreetMapLink

	print "instantiating EventInfoExtractors.OpenStreetMapLink"
	return OpenStreetMapLink()

def UIParameterOption_EventInfo_WikiLink(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.WikiLink import WikiLink

	print "instantiating EventInfoExtractors.WikiLink"
	return WikiLink()

def UIParameterOption_EventInfo_ContributorNumber(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.UserCount import UserCount

	print "instantiating EventInfoExtractors.UserCount"
	return UserCount()

def UIParameterOption_EventInfo_ContentPreview(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.ContentPreview import ContentPreview

	print "instantiating EventInfoExtractors.ContentPreview"
	return ContentPreview()

def UIParameterOption_EventInfo_RelatedWords(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.RelatedWords import RelatedWords

	importance = params["importance"]
	falloff = params["falloff"]

	print "instantiating EventInfoExtractors.RelatedWords: ", importance, falloff
	return RelatedWords(importance, falloff)

def UIParameterOption_EventInfo_RelatedPlaces(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.RelatedItems import RelatedItems

	print "instantiating EventInfoExtractors.RelatedItems"
	return RelatedItems()

def UIParameterOption_EventInfo_OccurrencePattern(params, alreadyConstructedParams):
	from Toolkit.EventInfoExtractors.OccurrencePattern import OccurrencePattern

	print "instantiating EventInfoExtractors.OccurrencePattern"
	return OccurrencePattern()





###
###
### FILTERS
###
###
def UIParameterOption_DataFilters_RemoveSimilarsFromSameUser(params, alreadyConstructedParams):
	from Toolkit.DataFilters.RemoveSimilarsFromSameUser import RemoveSimilarsFromSameUser

	timespan = params["timespan"]
	levenshtein_ratio = params["levenshtein_ratio"]

	print "instantiating data_filters.RemoveSimilarsFromSameUser with: ", (timespan, levenshtein_ratio)
	return RemoveSimilarsFromSameUser(timespan=timespan, levenshtein_ratio=levenshtein_ratio)
def UIParameterOption_ClusterFilters_RemoveUnrelatedPhotos(params, alreadyConstructedParams):
	from Toolkit.ClusterFilters.RemoveUnrelatedPhotos import RemoveUnrelatedPhotos

	print "instantiating cluster_filters.RemoveUnrelatedPhotos"
	return RemoveUnrelatedPhotos()

def UIParameterOption_EventFilters_ContributorNumberThreshold(params, alreadyConstructedParams):
	from Toolkit.EventFilters.ContributorNumberThreshold import ContributorNumberThreshold

	threshold = params["threshold"]

	print "instantiating event_filters.ContributorNumberThreshold with: ", (threshold,)
	return ContributorNumberThreshold(threshold)

def UIParameterOption_EventFilters_ContentClusterSizeThreshold(params, alreadyConstructedParams):
	from Toolkit.EventFilters.ContentClusterSizeThreshold import ContentClusterSizeThreshold

	threshold = params["threshold"]

	print "instantiating event_filters.ContentClusterSizeThreshold with: ", (threshold,)
	return ContentClusterSizeThreshold(threshold)

def UIParameterOption_EventFilters_GaussianShapeFilter(params, alreadyConstructedParams):
	from Toolkit.EventFilters.GaussianShapeFilter import GaussianShapeFilter
	match_ratio = params["match_ratio"]

	print "instantiating event_filters.GaussianShapeFilter with: ", (match_ratio,)
	return GaussianShapeFilter(match_ratio)





###
###
### ADDITIONAL
###
###
def UIParameterOption_TextFilters_NumericFilter(params, alreadyConstructedParams):
	from inc.word_filters import NumericFilter

	print "instantiating inc.word_filters.NumericFilter"
	return NumericFilter()

def UIParameterOption_TextFilters_MinLengthFilter(params, alreadyConstructedParams):
	from inc.word_filters import MinLengthFilter

	minLength = params["minLength"]

	print "instantiating inc.word_filters.MinLengthFilter with: ", minLength
	return MinLengthFilter(minLength)