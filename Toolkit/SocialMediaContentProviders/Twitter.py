from twython import Twython

from FrameworkModels import BasicContent, SocialMediaContentProvider
from inc import helper, algos
import sqlite3
import datetime
from dateutil.relativedelta import relativedelta
from dateutil import parser
from dateutil import tz
import time


APP_KEY = 'OCaxyFBTf0ePtcIcvNqAzYpNR'
APP_SECRET = 'pNWFrZmoEL3qWgSIwrdaLx0oIc1hN3BNNo8XKfsxxk7PxIAS21'
FETCH_COUNT = 100
TWEET_URL = "https://twitter.com/{user}/status/{tweet_id}"

class TwitterTweet(BasicContent):
	def __init__(self, dbtweet):
		super(TwitterTweet, self).__init__(dbtweet['id'], dbtweet['text'], dbtweet['timestamp'], helper.GeoLocation(dbtweet['geo_latitude'], dbtweet['geo_longitude']), dbtweet['owner'], dbtweet['url'])

class TwitterContentProvider(SocialMediaContentProvider):
	def __init__(self, dt_range, geo_bb, query, dbfile="../Toolkit/SocialMediaContentProviders/Twitter.sqlite"):
		if dt_range.start <= datetime.datetime.now()-relativedelta(days=7):
			raise Exception("Twitter only supports querying data for the last 7 days.")

		super(TwitterContentProvider, self).__init__(dt_range, geo_bb)

		self.query = query

		self.dbcon = sqlite3.connect(dbfile)
		self.dbcon.row_factory = sqlite3.Row


	def GetContent(self):
		data = []
		
		self._load_data()

		dbres = self.dbcon.execute("\
			SELECT\
				id, owner, text, timestamp, geo_latitude, geo_longitude, url\
			FROM\
				twitter_tweets\
			WHERE\
				timestamp>=? AND timestamp<=? AND geo_latitude>=? AND geo_latitude<=? AND geo_longitude>=? AND geo_longitude<=?\
			",

			(helper.datetime_to_ts(self.DateTimeRange.start), helper.datetime_to_ts(self.DateTimeRange.end), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
		)

		for row in dbres:
			data.append(TwitterTweet(row))

		return data
	
	def _load_data(self):
		#print "downloading data.."

		## INITIALIZE TWITTER API ##
		self.twitter = Twython(APP_KEY, APP_SECRET, oauth_version=2)
		ACCESS_TOKEN = self.twitter.obtain_access_token()

		self.twitter = Twython(APP_KEY, access_token=ACCESS_TOKEN)
		###########################
		
		##### DOWNLOAD TWEETS #####
		## assemble parameters
		param_q = self.query
		param_q += " since:" + self.DateTimeRange.start.strftime("%Y-%m-%d")
		param_q += " until:" + self.DateTimeRange.end.strftime("%Y-%m-%d")
		
		geo_lat_center = self.GeoBoundingBox.latitude[0] + (self.GeoBoundingBox.latitude[1]-self.GeoBoundingBox.latitude[0])/2.
		geo_lon_center = self.GeoBoundingBox.longitude[0] + (self.GeoBoundingBox.longitude[1]-self.GeoBoundingBox.longitude[0])/2.
		geo_radius = max(algos.latlon_distance((self.GeoBoundingBox.latitude[0], geo_lon_center), (self.GeoBoundingBox.latitude[1], geo_lon_center)), algos.latlon_distance((geo_lat_center, self.GeoBoundingBox.longitude[0]), (geo_lat_center, self.GeoBoundingBox.longitude[1])))
		param_geocode = str(geo_lat_center) + "," + str(geo_lon_center) + "," + str(geo_radius) + "km"
		
		#print "\t\t", param_q, param_geocode

		## check if an old download is to continue
		fetchIndex = self.dbcon.execute("\
			SELECT\
				id_min, id_max\
			FROM\
				twitter_tweets_index\
			WHERE\
				query=?\
				AND timestamp_low IS NOT NULL AND timestamp_low<=?\
				AND timestamp_high IS NOT NULL AND timestamp_high>=?\
				AND geo_latitude_low<=?\
				AND geo_latitude_high>=?\
				AND geo_longitude_low<=?\
				AND geo_longitude_high>=?\
			",

			(self.query, helper.datetime_to_ts(self.DateTimeRange.start), helper.datetime_to_ts(self.DateTimeRange.end), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
		).fetchone()

		## download
		# check for newer entries
		if not(fetchIndex is None):
			#print "downloading NEW ones"
			self._download(q=param_q, geocode=param_geocode, since_id=fetchIndex["id_max"], fetchIndex=fetchIndex)
			#print "..finished downloading NEW ones"

		# download rest
		self._download(q=param_q, geocode=param_geocode, max_id=(None if fetchIndex is None else fetchIndex["id_min"]-1), fetchIndex=fetchIndex)
	
		print "\n"

	def _download(self, q, geocode, since_id=None, max_id=None, count=FETCH_COUNT, fetchIndex=None):
		i = 0
		while True:
			# fetch one page
			results = self.twitter.search(q=q, geocode=geocode, since_id=since_id, max_id=max_id, count=count)
			if len(results["statuses"]) <= 0:
				break
			print len(results["statuses"]), results["search_metadata"]
			print "fetching page:", i, "since_id=", since_id, "max_id=", max_id
			i += 1

			# save page to db
			for tweet in results["statuses"]:
				dtCreatedAt = parser.parse(tweet["created_at"]).astimezone(tz.tzlocal()).replace(tzinfo=None)
				geoLocation = None if tweet["coordinates"] is None else (tweet["coordinates"]["coordinates"][1], tweet["coordinates"]["coordinates"][0])

				if dtCreatedAt < self.DateTimeRange.start or dtCreatedAt > self.DateTimeRange.end:
					continue
				if geoLocation is None or geoLocation[0] < self.GeoBoundingBox.latitude[0] or geoLocation[0] > self.GeoBoundingBox.latitude[1] or geoLocation[1] < self.GeoBoundingBox.longitude[0] or geoLocation[1] > self.GeoBoundingBox.longitude[1]:
					continue

				try:
					print "\t\t---> INSERT: ", tweet["id"], tweet["text"]
					url = TWEET_URL.format(user=str(tweet["user"]["screen_name"]), tweet_id=str(tweet["id_str"]))
					self.dbcon.execute('DELETE FROM twitter_tweets WHERE id=?', (int(tweet["id"]),))
					self.dbcon.execute('INSERT INTO twitter_tweets(id, owner, text, timestamp, geo_latitude, geo_longitude, url) VALUES(?,?,?,?,?,?,?)', (int(tweet["id"]), int(tweet["user"]["id"]), tweet["text"], helper.datetime_to_ts(dtCreatedAt), float(geoLocation[0]), float(geoLocation[1]), url))
				except:
					import traceback
					print "\t\tinsert failed:", traceback.format_exc()
			self.dbcon.commit()

			# update index
			if fetchIndex is None:
				fetchIndex = {
					"id_min": results["statuses"][0]["id"], 
					"id_max": results["statuses"][-1]["id"], 
					"query": self.query, 
					"timestamp_low": helper.datetime_to_ts(self.DateTimeRange.start), 
					"timestamp_high": helper.datetime_to_ts(self.DateTimeRange.end), 
					"geo_latitude_low": self.GeoBoundingBox.latitude[0], 
					"geo_latitude_high": self.GeoBoundingBox.latitude[1], 
					"geo_longitude_low": self.GeoBoundingBox.longitude[0], 
					"geo_longitude_high": self.GeoBoundingBox.longitude[1]
				}
				self.dbcon.execute("\
					INSERT INTO\
						twitter_tweets_index(id_min, id_max, query, timestamp_low, timestamp_high, geo_latitude_low, geo_latitude_high, geo_longitude_low, geo_longitude_high)\
					VALUES\
						(?,?,?,?,?,?,?,?,?)\
					",

					(fetchIndex["id_min"], fetchIndex["id_max"], fetchIndex["query"], fetchIndex["timestamp_low"], fetchIndex["timestamp_high"], fetchIndex["geo_latitude_low"], fetchIndex["geo_latitude_high"], fetchIndex["geo_longitude_low"], fetchIndex["geo_longitude_high"])
				)
				self.dbcon.commit()
			else:
				fetchIndex_id_min = fetchIndex["id_min"]
				fetchIndex_id_max = fetchIndex["id_max"]

				if results["statuses"][-1]["id"] < fetchIndex_id_min:
					fetchIndex_id_min = results["statuses"][-1]["id"]
				if results["statuses"][0]["id"] > fetchIndex_id_max:
					fetchIndex_id_max = results["statuses"][0]["id"] # TODO: this marks all tweets below that id as fetched => if the API crashes while fetching newer entries all possibly not fetched tweets between that id and the old one are lost

				self.dbcon.execute("\
					UPDATE\
						twitter_tweets_index\
					SET\
						id_min=?,\
						id_max=?\
					WHERE\
						query=?\
						AND timestamp_low IS NOT NULL AND timestamp_low<=?\
						AND timestamp_high IS NOT NULL AND timestamp_high>=?\
						AND geo_latitude_low<=?\
						AND geo_latitude_high>=?\
						AND geo_longitude_low<=?\
						AND geo_longitude_high>=?\
					",
					(fetchIndex_id_min, fetchIndex_id_max, self.query, helper.datetime_to_ts(self.DateTimeRange.start), helper.datetime_to_ts(self.DateTimeRange.end), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1]))
				self.dbcon.commit()

			# abort if everything is fetched
			if len(results["statuses"]) < results["search_metadata"]["count"]:
				break

			# continue with next page
			max_id = results["statuses"][-1]["id"] - 1