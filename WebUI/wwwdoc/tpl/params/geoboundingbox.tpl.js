define(["text!tpl/params/geoboundingbox.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;

			// set up google maps
			var mapOptions = {
				center: new google.maps.LatLng(view.latitude[0]+(view.latitude[1]-view.latitude[0])/2, view.longitude[0]+(view.longitude[1]-view.longitude[0])/2),
				zoom: 9
			};
			var map = new google.maps.Map($(this).find(".map").get(0), mapOptions);
			$(this).find(".map").resizable({
				"handles": "s",
				stop: function(event, ui) {
					google.maps.event.trigger(map, "resize");
				}
			});

			// set up editable rectangle
			var mapBounds = new google.maps.LatLngBounds(
				new google.maps.LatLng(view.latitude[0], view.longitude[0]),
				new google.maps.LatLng(view.latitude[1], view.longitude[1])
			);
			map.fitBounds(mapBounds);
			
			this.rectangle = new google.maps.Rectangle({
				bounds: mapBounds,
				editable: true,
				draggable: true,
			});
			this.rectangle.setMap(map);

			// Create the search box and link it to the UI element.
			var input = /** @type {HTMLInputElement} */($(this).find(".map-pac-input").get(0));
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

			var searchBox = new google.maps.places.SearchBox(/** @type {HTMLInputElement} */(input));

			// Listen for the event fired when the user selects an item from the
			// pick list. Retrieve the matching places for that item.
			var markers=[];
			google.maps.event.addListener(searchBox, 'places_changed', function() {
				var places = searchBox.getPlaces();

				for (var i = 0, marker; marker = markers[i]; i++) {
					marker.setMap(null);
				}

				// For each place, get the icon, place name, and location.
				markers = [];
				var bounds = new google.maps.LatLngBounds();
				for (var i = 0, place; place = places[i]; i++) {
					/*var image = {
						url: place.icon,
						size: new google.maps.Size(71, 71),
						origin: new google.maps.Point(0, 0),
						anchor: new google.maps.Point(17, 34),
						scaledSize: new google.maps.Size(25, 25)
					};

					// Create a marker for each place.
					var marker = new google.maps.Marker({
						map: map,
						icon: image,
						title: place.name,
						position: place.geometry.location
					});

					markers.push(marker);*/

					bounds.extend(place.geometry.location);
				}

				map.fitBounds(bounds);
			});

			// Bias the SearchBox results towards places that are within the bounds of the
			// current map's viewport.
			google.maps.event.addListener(map, 'bounds_changed', function() {
				var bounds = map.getBounds();
				searchBox.setBounds(bounds);
			});

			// hackfix: dynamically adding multiple map instances (as it happens on this page) screws the map viewport
			var initialized=false;
			google.maps.event.addListener(map, "idle", function() {
				if (!initialized) {
					google.maps.event.trigger(map, "resize");
					map.setCenter(mapOptions.center);
					map.fitBounds(mapBounds);
					initialized = true;
				}
			});


			// hackfix: foreward map's parent resize [resizing the splitter pannel does not automatically propagate to the map]
			$(".map").parents().live("resize", function() {
				// ATTENTION: $(".map") incidentally generic and not containing a reference to this (otherwise .live does not work)
				// ATTENTION: possible memory/performance leak when multiple maps are opened and the live trigger add up
				google.maps.event.trigger(map, "resize");
			});

			// reset-bbox button
			$(this).find(".map-reset-bbox").click(function(eve) {
				var mBounds = map.getBounds();
				var ne = mBounds.getNorthEast();
				var sw = mBounds.getSouthWest();
				
				var rectangleToMapRatio = 0.6;
				self.rectangle.setBounds(new google.maps.LatLngBounds(
					new google.maps.LatLng(sw.lat() + (1-rectangleToMapRatio)/2*(ne.lat()-sw.lat()), sw.lng() + (1-rectangleToMapRatio)/2*(ne.lng()-sw.lng())),
					new google.maps.LatLng(ne.lat() - (1-rectangleToMapRatio)/2*(ne.lat()-sw.lat()), ne.lng() - (1-rectangleToMapRatio)/2*(ne.lng()-sw.lng()))
				));

				eve.preventDefault();
			});
		}, {
			getValue: function() {
				var bounds = this.rectangle.getBounds();
				var ne = bounds.getNorthEast();
				var sw = bounds.getSouthWest();
				return {
					"latitude": [sw.lat(), ne.lat()],
					"longitude": [sw.lng(), ne.lng()]
				};
			}
		}
	);

});