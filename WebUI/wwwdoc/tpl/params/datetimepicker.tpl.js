define(["text!tpl/params/datetimepicker.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			$(this).find("input").datetimepicker();
			if (view.default != null) 
				$(this).find("input").val(view.default);
		}, {
			getValue: function() {
				return $(this).find("input").val();
			}
		}
	);

});