define(["text!tpl/tab.main.param.param.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			this.name = view.name;
			
			var param = param_tpls[view.template].render(view.view);
			this.param = param;
			$(this).find(".uiparam-param-container").append(param);
		}, {
			name: null,
			param: null,

			getValue: function() {
				return this.param.getValue();
			}
		}
	);

});