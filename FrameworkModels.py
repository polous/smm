###
###
### DATA MODELS
###
###
class BasicContent(object):
	"""
		Represents a content entity from an arbitrary social media platform.
	"""

	def __init__(self, id, title, timestamp, geo_location, publisher, url):
		"""
			Initializes this content entity.

			Parameters:
				id: 			unique entity identifier 	any
				title:			arbitrary string			str
				timestamp:		timestamp 					int
				geo_location:	geo location 				helper.GeoLocation
				publisher: 		publisher 					any
				url: 			unique resource locator 	str
		"""
		self.Id = id
		self.Title = title
		self.TimeStamp = timestamp
		self.GeoLocation = geo_location
		self.Publisher = publisher
		self.URL = url


class ContentCluster(list):
	"""
		Represents a content cluster identified from an EventDetectionAlgorithm as forming an event.
	"""

	def __init__(self, contentCluster, additionalData):
		"""
			Initializes this content-cluster entity.

			Parameters:
				contentCluster 	the cluster of content 			List<BasicContent>
				additionalData 	arbitrary additional data 		any 
		"""
		list.__init__(self, contentCluster)
		self.AdditionalData = additionalData


class BasicEvent(object):
	"""
		Represents a detected event.
	"""

	def __init__(self, identifier, time, location, contentCluster, additionalInfo = None):
		"""
			Initializes this event entity.

			Parameters:
				identifier 			arbitrary identifier for this event (usually the name) 			str
				time 				the estimated timespan in which this event took place  			(DateTime start, DateTime end)
				location 			the estimated location of this event 							{center: (lat, long), bbox: {latitude: (min_lat, max_lat), longitude: (min_long, max_long)}}
				contentCluster 		the content cluster this event origins from 					ContentCluster
				additionalInfo 		arbitrary additional key-value info  							{}
		"""

		self.Identifier = identifier
		self.Time = {
			"start": time[0],
			"end": time[1]
		}
		self.Location = {
			"center": {
				"latitude": location["center"][0],
				"longitude": location["center"][1],
			},
			"bbox": {
				"latitude": (location["bbox"]["latitude"][0], location["bbox"]["latitude"][1]),
				"longitude": (location["bbox"]["longitude"][0], location["bbox"]["longitude"][1])
			}
		}

		self.ContentCluster = contentCluster

		self.AdditionalInfo = {} if additionalInfo is None else additionalInfo

	def to_JSONCompatibleObject(self):
		"""
			Converts this object into an JSON compatible format.
		"""

		""" HACK FIX: better use custom jsonEncoder """
		from uiinc.ExecThread import _UIProxy_Info

		eventDict = {
			"name": self.Identifier,
			"time": {
				"start": self.Time["start"].isoformat(),
				"end": self.Time["end"].isoformat(),
			},
			"place": self.Location,
			"info": {},
			"cluster_size": len(self.ContentCluster)
		}
		for key in self.AdditionalInfo:
			val = self.AdditionalInfo[key]

			if isinstance(val, _UIProxy_Info):
				eventDict["info"][key] = {
					"name": val.uiEventInfoName,
					"value": val.value if self._isJsonCompatible(val.value) else str(val.value)
				}
			else:
				eventDict["info"][key] = val if self._isJsonCompatible(val) else str(val)

		return eventDict

	def _isJsonCompatible(self, val):
		"""
			Helper: verifies whether an object is directly convertible into json or not
		"""

		import json

		try:
			json.dumps(val)
			return True
		except:
			return False





###
###
### PROCESS INTERFACES
###
###
class SocialMediaContentProvider(object):
	"""
		Acquires content from a social media platform.
	"""

	def __init__(self, dt_range, geo_bb):
		"""
			Initializes this content provider.

			Parameters:
				dt_range:	date time range 	helper.DateTimeRange
				geo_bb:		geo bounding box 	helper.GeoBoundingBox
		"""
		self.DateTimeRange = dt_range
		self.GeoBoundingBox = geo_bb

	def GetContent(self):
		"""
			Retrieves all content this provider offers (filters may have been applied in the constructor).

			Returns:
				List<BasicContent>
		"""
		raise Exception("abstract method")


class EventDetectionAlgorithm(object):
	"""
		Detects event content clusters in the given social media content.
	"""

	def DetectEvents(self, content, contentProvider):
		"""
			Parameters:
				content 			the content to process 						List<BasicContent>
				contentProvider 	the provider the content origins from		SocialMediaContentProvider
			Returns:
				List<ContentCluster>
		"""
		raise Exception("abstract method")


class EventInfoExtractor(object):
	"""
		Extracts information from content clusters related to the detected events.
	"""

	def ExtractInfo(self, content, contentCluster, contentProvider, event, events):
		"""
			Parameters:
				content 			the raw content used for processing 								List<BasicContent>
				contentCluster 		the content cluster to process 										ContentCluster
				contentProvider 	the content provider the content cluster origins from 				SocialMediaContentProvider
				event 				the most recent version of the current event to be extended 		BasicEvent
				events 				a list of all detected events (their most recent version) 			List<BasicEvent>
		"""
		raise Exception("abstract method")

class EventIdentifierExtractor(EventInfoExtractor):
	"""
		Extracts an identifier from a content cluster for a detected event.
	"""

	def ExtractInfo(self, content, contentCluster, contentProvider, event, events):
		"""
			Parameters:
				content 			the raw content used for processing 								List<BasicContent>
				contentCluster 		the content cluster to process 										ContentCluster
				contentProvider 	the content provider the content cluster origins from 				SocialMediaContentProvider
				event 				the most recent version of the current event to be extended 		BasicEvent
				events 				a list of all detected events (their most recent version) 			List<BasicEvent>

			Returns:
				str
		"""
		raise Exception("abstract method")

class EventTimeExtractor(EventInfoExtractor):
	"""
		Extracts the time from a content cluster during which the event took place.
	"""

	def ExtractInfo(self, content, contentCluster, contentProvider, event, events):
		"""
			Parameters:
				content 			the raw content used for processing 								List<BasicContent>
				contentCluster 		the content cluster to process 										ContentCluster
				contentProvider 	the content provider the content cluster origins from 				SocialMediaContentProvider
				event 				the most recent version of the current event to be extended 		BasicEvent
				events 				a list of all detected events (their most recent version) 			List<BasicEvent>

			Returns:
				(DateTime start, DateTime end)
		"""
		raise Exception("abstract method")

class EventLocationExtractor(EventInfoExtractor):
	"""
		Extracts the location from a content where the event took place.
	"""

	def ExtractInfo(self, content, contentCluster, contentProvider, event, events):
		"""
			Parameters:
				content 			the raw content used for processing 								List<BasicContent>
				contentCluster 		the content cluster to process 										ContentCluster
				contentProvider 	the content provider the content cluster origins from 				SocialMediaContentProvider
				event 				the most recent version of the current event to be extended 		BasicEvent
				events 				a list of all detected events (their most recent versions) 			List<BasicEvent>

			Returns:
				{center: (lat, long), bbox: {latitude: (min_lat, max_lat), longitude: (min_long, max_long)}}
		"""
		raise Exception("abstract method")





###
###
### FILTER INTERFACES
###
###
class DataFilter(object):
	"""
		Filters raw data.
		Filtering stage applied to the raw data.
	"""

	def filter(self, datum):
		"""
			Params:
				datum:		datum in question

			Returns:
				TRUE to remove datum || FALSE to keep datum
		"""

		raise "not implemented"

	def reset(self):
		"""
			Resets this filter for the application to new data.
			Implement in order to reset any counting state or similar..
		"""
		pass

	def __str__(self):
		return self.__class__.__name__


class ClusterFilter(object):
	"""
		Filters content clusters identified by an event detection algorithm to form an event.
		Filtering stage directly following the event detection.
	"""

	def filter(self, cluster):
		"""
			Params:
				cluster:			ContentCluster object of the cluster in question
			Returns:
				None to remove cluster || filteredCluster otherwise
		"""
		raise "not implemented"

	def reset(self):
		"""
			Resets this filter for the application to new data.
			Implement in order to reset any counting state or similar..
		"""
		pass

	def __str__(self):
		return self.__class__.__name__


class EventFilter(object):
	"""
		Filters BasicEvents.
		Last applied filtering stage.
	"""

	def filter(self, event):
		"""
			Params:
				event:		BasicEvent object containing event information
			Returns:
				TRUE to remove event from list, FALSE to keep it
		"""
		raise "not implemented"

	def reset(self):
		"""
			Resets this filter for the application to new data.
			Implement in order to reset any counting state or similar..
		"""
		pass

	def __str__(self):
		return self.__class__.__name__