import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventInfoExtractor

class OccurrencePattern(EventInfoExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		CONFIG_EVENT_INTERVALS = {
			"weekly": (60*60*24*5, 60*60*24*9),
			"yearly": (60*60*24*345, 60*60*24*380),
		}

		fittingEvents = []
		for EVENT2 in events:
			if EVENT2.Identifier == event.Identifier:
				fittingEvents.append(EVENT2)

		pattern = None
		if len(fittingEvents) > 1:
			avgTemporalDistance = 0
			for i in range(1, len(fittingEvents)):
				avgTemporalDistance += ((fittingEvents[i].Time["start"]+(fittingEvents[i].Time["end"]-fittingEvents[i].Time["start"])/2) - (fittingEvents[i-1].Time["start"]+(fittingEvents[i-1].Time["end"]-fittingEvents[i-1].Time["start"])/2)).total_seconds()
				avgTemporalDistance /= len(fittingEvents)-1

			for interval in CONFIG_EVENT_INTERVALS:
				if avgTemporalDistance >= CONFIG_EVENT_INTERVALS[interval][0] and avgTemporalDistance <= CONFIG_EVENT_INTERVALS[interval][1]:
					pattern = interval
					break
		return pattern

	def __str__(self):
		return "OccurrencePattern"