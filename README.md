Social Media Monitoring - Clusters & Filters
==============

Python library in Python 2.7, including a web-based graphical user interface .

Dependencies
--------------
numpy, scikit-learn, python-levenshtein, pandas, statsmodels, cherrypy, jinja2, patsy, flickrapi, twython, python-instagram

How To Run
--------------
1. download sourcecode
2. resolve dependencies (e.g.: via pip; open up a command-line and enter: "pip install -U numpy scikit-learn python-levenshtein pandas statsmodels cherrypy jinja2 patsy flickrapi twython python-instagram")
3. run ./WebUI/appserver.py (e.g.: open up a command-line, navigate to the ./WebUI directory and run "python appserver.py")
4. open your favorite web-browser and navigate to "http://localhost:8080"
5. verify that everything is set up correctly by running the algorithm with its default parameters, at least the event "oktoberfest" should be detected
6. play around with the parameters and enjoy the application :)

Troubleshooting
--------------
When encountering any problems with the social media providers, try running "test_connections.py" in a terminal and follow its instructions.