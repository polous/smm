define(["text!tpl/tab.main.group.tpl.html", "tpl/tab.main.param.tpl.js"], function(tpl, mainParamTpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self = this;

			this.name = view.name;
			this.view = view;

			$(this).find(".ui-widget-header").click(function() {
				var body = $(this).siblings(".ui-widget-content");
				body.slideToggle("fast");
				/*if (body.is(":visible")) {
					$(this).addClass("active");
					body.hide();
				} else {
					body.show();
					$(this).removeClass("active");
				}*/
			});

			$(this).find(".uiparam-group-content").droppable({
			  greedy: true,
			  activeClass: "active",
			  accept: function(draggable) {
				return draggable.data("uiparam-group") == view.optionGroup;
			  },
			  drop: function(event, ui) {
			  	self.addParam(ui.draggable.data("uiparam-group"), ui.draggable.data("uiparam-option"));
			  }
			});
		}, {
			name: null,
			params: [],

			addParam: function(group, option) {
				var self = this;

				if (this.view.optionCountMax!=null && this.params.length >= this.view.optionCountMax) {
			  		if (self.params.length > 0) {
				  		$(self.params.shift()).remove();
				  	}
			  	}

			  	if (this.view.optionCountMax==null || this.params.length < this.view.optionCountMax) {
			  		var param = mainParamTpl.render(UIPARAMS_OPTIONS[group].options[option]);
			  		self.params.push(param);
			  		$(param).on("close", function(event) {
			  			for (var i=0; i<self.params.length; i++) {
			  				if (self.params[i] == param) {
			  					self.params.splice(i, 1);
			  					param.remove();
			  				}
			  			}
			  			event.stopPropagation();
			  		});
			  		$(self).find(".uiparam-group-params").first().append(param);
			  	}
			}
		}
	);

});