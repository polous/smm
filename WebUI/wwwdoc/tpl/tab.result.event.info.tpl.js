define(["text!tpl/tab.result.event.info.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			if (EVENTINFO[view.name] != null) {
				$(this).find(".view-result-event-info-value").append(eventinfo_tpls[EVENTINFO[view.name]].render(view.value))
			} else {
				$(this).find(".view-result-event-info-value").text(view.value);
			}
		}, {
		},
		function(view) {
			var alteredView = {
				"label": UIPARAMS_OPTIONS.eventInfo.options[view.name].label
			};
			return alteredView;
		}
	);

});