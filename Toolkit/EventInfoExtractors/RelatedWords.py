import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventInfoExtractor
from inc import helper

class RelatedWords(EventInfoExtractor):
	def __init__(self, importance, falloff):
		self.importance = importance
		self.falloff = falloff

	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		from inc import algos
		avgWordUsage = algos.get_averageWordUsage(data)

		related_words = []

		from collections import OrderedDict
		cluster_words = OrderedDict() # use OrderedDict in order to prevent in-sentence-positioniong of words that share the exact same per-photo-usage [for strongly related words like "britney" and "spears"]
		for photo in event.ContentCluster:
			words = photo.Title.split(" ")
			for word in words:
				word = helper.filter_word(word)
				if not(word is None):
					if not(word in cluster_words):
						cluster_words[word] = 0
					cluster_words[word] += 1
		for cluster_word in cluster_words:
			cluster_words[cluster_word] = float(cluster_words[cluster_word]) / float(len(event.ContentCluster))

		sorted_words = sorted(cluster_words.iteritems(), key=lambda x: -1*x[1]) # sort words according to per-photo-usage in descending order

		highestRelatedWordUsage = None
		for sorted_word in sorted_words:
			if not(highestRelatedWordUsage is None) and sorted_word[1] < self.falloff*highestRelatedWordUsage:
				break
			if sorted_word[1]*(1-self.importance) > avgWordUsage[sorted_word[0]]:
				related_words.append(sorted_word[0])
				if highestRelatedWordUsage is None:
					highestRelatedWordUsage = sorted_word[1]

		return related_words

	def __str__(self):
		return "RelatedWords"