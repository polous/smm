define(["text!tpl/tab.result.event.tpl.html", "tpl/tab.result.event.info.tpl.js"], function(tpl, eventInfoTpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self=this;

			/** add info **/
			for (var i in view.info) {
				eventInfo = view.info[i]
				$(this).find(".event-info").append(eventInfoTpl.render(eventInfo));
			}

			/** make event box expandable **/
			var initialized=false;
			$(this).find(".event-header").click(function() {
				var body = $(self).find(".event-body");
				if (body.is(":visible")) {
					body.hide();
				} else {
					if (!initialized) {
						initialized = true;

						// set up google maps
						var mapOptions = {
							center: new google.maps.LatLng(view.place.center.latitude, view.place.center.longitude),
							zoom: 9
						};
						var map = new google.maps.Map($(self).find(".event-map").get(0), mapOptions);
						$(self).find(".event-map").resizable({
							"handles": "s",
							stop: function(event, ui) {
								google.maps.event.trigger(map, "resize");
							}
						});

						// set up marker
						var marker = new google.maps.Marker({
							position: new google.maps.LatLng(view.place.center.latitude, view.place.center.longitude),
							map: map,
							title: view.name
						});

						// set up rectangle
						var mapBounds = new google.maps.LatLngBounds(
							new google.maps.LatLng(view.place.bbox.latitude[0], view.place.bbox.longitude[0]),
							new google.maps.LatLng(view.place.bbox.latitude[1], view.place.bbox.longitude[1])
						);
						map.fitBounds(mapBounds);

						var rectangle = new google.maps.Rectangle({
							bounds: mapBounds
						});
						rectangle.setMap(map);

						// hackfix: dynamically adding multiple map instances (as it happens on this page) screws the map viewport
						var initialized=false;
						google.maps.event.addListener(map, "idle", function() {
							if (!initialized) {
								google.maps.event.trigger(map, "resize");
								map.setCenter(mapOptions.center);
								map.fitBounds(mapBounds);
								initialized = true;
							}
						});
					}
					body.show();
				}
			});
		}, {
		},
		function(view) {
			var alteredView = {
				"name": view.name,
				"dt_start": view.time.start,
				"dt_end": view.time.end
			};
			return alteredView;
		}
	);

});