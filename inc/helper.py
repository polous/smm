import time, datetime
import os
import numpy as np
import math
import copy

import refl


class DateTimeRange(object):
	def __init__(self, start, end):
		self.start = start
		self.end = end

class GeoBoundingBox(object):
	def __init__(self, lat_min, long_min, lat_max, long_max):
		self.latitude = (lat_min, lat_max)
		self.longitude = (long_min, long_max)

class GeoLocation(object):
	def __init__(self, latitude, longitude):
		self.latitude = latitude
		self.longitude = longitude

def get_feature_data(candidate, features, aggregation_spans, db):
	feature_ids = [f.__name__ for f in features]
	feature_ids_hashmap = {}
	fid = 0
	for f in feature_ids:
		feature_ids_hashmap[f] = fid
		fid += 1
	time_spans = {}
	geo_lat_spans = {}
	geo_long_spans = {}
	for span in aggregation_spans:
		time_spans[str(span.ts_span[0])+"-"+str(span.ts_span[1])] = span.ts_span_id
		geo_lat_spans[str(span.geo_lat_span[0])+"-"+str(span.geo_lat_span[1])] = span.geo_lat_span_id
		geo_long_spans[str(span.geo_long_span[0])+"-"+str(span.geo_long_span[1])] = span.geo_long_span_id

	data = []
	dbres = db.execute("SELECT time_ts_span, geo_latitude_span, geo_longitude_span, feature_id, feature FROM features WHERE word=? AND feature_id in ({})".format(",".join("?"*len(feature_ids))), [candidate]+feature_ids)
	for row in dbres:
		datum = [time_spans[row[0]], geo_lat_spans[row[1]], geo_long_spans[row[2]]] + [0 for i in range(len(feature_ids))]
		datum[3+feature_ids_hashmap[row[3]]] = row[4]
		data.append(datum)
	return np.asarray(data)

"""
	filters special characters from a word

	Params:
		{string} word

	Returns:
		{string} character filtered word OR None if the whole word was filtered
"""
def filter_word(word):
	fword = word.lower().strip(" ,.:;-\"'!?<>()[]{}#@/&|$=\\")
	"""
	try:
		int(fword)
		return None
	except:
		pass
	"""
	return None if fword=="" else fword

"""
	HELPER: generates aggregation spans

	Params:
		{int,int,int} t_aggr 					temporal aggregation (start timestamp, end timestamp, aggregation span in seconds)
		{float,float,float} geo_aggr_lat 		geo latitude aggregation (start latitude, end latitude, aggregation span in degrees) [resolution of 1./3600 = 1 arcsecond]
		{float,float,float} geo_aggr_long 		geo longitude aggregation (start latitude, end latitude, aggregation span in degrees) [resolution of 1./3600 = 1 arcsecond]
	Returns:
		list of AggregationSpan objects
"""
def aggregation_spans(t_aggr, geo_aggr_lat, geo_aggr_long):
	aggregation_spans = []

	# temporal aggregation
	t_aggregation_steps = int(math.ceil((t_aggr[1]-t_aggr[0]+1)/float(t_aggr[2])))
	for i in range(t_aggregation_steps):
		start = t_aggr[0] + i*t_aggr[2]
		end = start + t_aggr[2] - 1
		if end > t_aggr[1]:
			end = t_aggr[1]
		aggregation_spans.append(AggregationSpan((start,end), i))

	# geo latitude aggregation
	new_aggregation_spans = []
	geo_lat_aggregation_steps = int(math.ceil((geo_aggr_lat[1]-geo_aggr_lat[0]+1./3600.) / geo_aggr_lat[2]))
	for i_span in range(len(aggregation_spans)):
		aggregation_span = aggregation_spans[i_span]

		for i in range(geo_lat_aggregation_steps):
			start = geo_aggr_lat[0] + i*geo_aggr_lat[2]
			end = start + geo_aggr_lat[2] - (1./3600.)
			if end > geo_aggr_lat[1]:
				end = geo_aggr_lat[1]
			new_aggregation_spans.append(AggregationSpan(aggregation_span.ts_span, aggregation_span.ts_span_id, (start, end), i))
	aggregation_spans = new_aggregation_spans

	# geo longitude aggregation
	new_aggregation_spans = []
	geo_long_aggregation_steps = int(math.ceil((geo_aggr_long[1]-geo_aggr_long[0]+1./3600.) / geo_aggr_long[2]))
	for i_span in range(len(aggregation_spans)):
		aggregation_span = aggregation_spans[i_span]

		for i in range(geo_long_aggregation_steps):
			start = geo_aggr_long[0] + i*geo_aggr_long[2]
			end = start + geo_aggr_long[2] - (1./3600.)
			if end > geo_aggr_long[1]:
				end = geo_aggr_long[1]
			new_aggregation_spans.append(AggregationSpan(aggregation_span.ts_span, aggregation_span.ts_span_id, aggregation_span.geo_lat_span, aggregation_span.geo_lat_span_id, (start, end), i))
	aggregation_spans = new_aggregation_spans

	return aggregation_spans

"""
	HELPER: keeps the results of aggregation_spans()
"""
class AggregationSpan(object):
	"""
		Params:
			{int,int OR None} ts_span 				timestamp span
			{int OR None} ts_span_id 				id for timestamp span
			{float,float OR None} geo_lat_span		latitude span
			{int OR None} geo_lat_span_id 			id for latitude span 
			{float,float OR None} geo_long_span 	longitude span
			{int OR None} geo_long_span_id 			id for longitude span
	"""
	def __init__(self, ts_span=None, ts_span_id=None, geo_lat_span=None, geo_lat_span_id=None, geo_long_span=None, geo_long_span_id=None):
		self.ts_span = None if ts_span is None else (ts_span[0], ts_span[1])
		self.ts_span_id = ts_span_id
		self.geo_lat_span = None if geo_lat_span is None else (geo_lat_span[0], geo_lat_span[1])
		self.geo_lat_span_id = geo_lat_span_id
		self.geo_long_span = None if geo_long_span is None else (geo_long_span[0], geo_long_span[1])
		self.geo_long_span_id = geo_long_span_id

"""
	HELPER: merges multiple features into one data array 

	Params:
		{..} feature_list		list of features (a feature is a list of lists [span-number, geo_latitude, geo_longitude, feature])

	Returns:
		list of lists [span-number, geo_latitude, geo_longitude, feature0, feature1, ..]
"""
def merge_features(feature_list):
	# initialize with first feature
	data = copy.deepcopy(feature_list[0])

	# extend data with other features
	for feature_i in range(1, len(feature_list)):
		feature = feature_list[feature_i]

		# extend data for one dimension
		for datum in data:
			datum.append(0)

		# add feature
		for datum in feature:
			# check if (time, geo_latitude, geo_longitude) combination is already in data
			index = None
			for i in range(len(data)):
				if data[i][0]==datum[0] and data[i][1]==datum[1] and data[i][2]==datum[2]:
					index = i
					break

			# add datum
			if index is None:
				newDatum = [datum[0], datum[1], datum[2]] + [0 for i in range(len(data[0])-3)] + [datum[-1]]
				data.append(newDatum)
			else:
				data[i][-1] = datum[-1]

	return data


def time_str_to_float(str, format):
	return time.mktime(datetime.datetime.strptime(str, format).timetuple())

def datetime_to_ts(dt):
	return time.mktime(dt.timetuple())
