define(["text!tpl/params/floatslider.tpl.html"], function(tpl) {
	return new Template(
		tpl, 
		function(view, partials) {
			var self=this;

			function updatePreview() {
				$(self).find(".uiparam-param-floatslider-preview").text(self.getValue());
			}

			$(this).find(".uiparam-param-floatslider").slider({
				value: view.default,
				step: view.step,
				min: view.range[0],
				max: view.range[1],
				slide: function(event, ui) {
					updatePreview();
				},
				change: function(event, ui) {
					updatePreview();
				}
			});
			updatePreview();
		}, {
			getValue: function() {
				return $(this).find(".uiparam-param-floatslider").slider("value");
			}
		}
	);

});