import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventInfoExtractor

class WikiLink(EventInfoExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		return ("http://en.wikipedia.org/wiki/"+(("%20".join(event.Identifier.split(" "))).capitalize())).encode("utf-8")

	def __str__(self):
		return "WikiLink"