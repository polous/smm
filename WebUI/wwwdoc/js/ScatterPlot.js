ScatterPlot = null;
$(function() {
	var scales = [];
	var axisKeys = ["x", "y", "z"]
	var axisRange = [0, 10]
	var sphereRadius = 0.1;

	var container = [];
	var x3ds = [];
	var scenes = [];
	var currentScene = 0;
	for (var i=0; i<10; i++) { // x3d elements need to be loaded in advance => load a certain number of elements to reuse later
		var div = $("<div/>").css("display", "none").appendTo($("body"));
		var parent = d3.select(div.get(0)).style('width', "500px").style('height', "500px");
		var x3d = parent.append("x3d")
						.style( "width", parseInt(parent.style("width"))+"px" )
						.style( "height", parseInt(parent.style("height"))+"px" )
						.style( "border", "none" );
		var scene = x3d.append("scene");
		scene.append("orthoviewpoint")
				.attr( "centerOfRotation", [5, 5, 5])
				.attr( "fieldOfView", [-5, -5, 15, 15])
				.attr( "orientation", [-0.5, 1, 0.2, 1.12*Math.PI/4])
				.attr( "position", [8, 4, 15]);

		container.push(div);
		x3ds.push(x3d);
		scenes.push(scene);
	}
	ScatterPlot = function(data, ranges, axisScales, labelCount, _axisKeys) {
		var div = container[currentScene].css("display", "none");
		var x3d = x3ds[currentScene];
		var scene = scenes[currentScene];
		if (++currentScene >= x3ds.length)
			currentScene = 0;

		ScatterPlot.data = data
		ScatterPlot.ranges = ranges
		ScatterPlot.axisScales = axisScales
		ScatterPlot.labelCount = labelCount
		ScatterPlot.axisKeys = _axisKeys
		axisKeys = _axisKeys

		pdata = [];
		for (var i in data) {
			var datum = {}
			datum[axisKeys[0]] = data[i][0];
			datum[axisKeys[1]] = data[i][1];
			datum[axisKeys[2]] = data[i][2];
			datum["label"] = data[i][3];
			pdata.push(datum);
		}

		plot_initializePlot(scene, ranges, axisScales);
		plot_updateData(pdata, labelCount, x3d, scene);

		return div.detach().show();
	}
	ScatterPlot.getColorForLabel = function(label, labelCount) {
		return getColorAtScalar(label, labelCount);
	}

	function getColorAtScalar(n, maxLength) {
		var h = ((maxLength-n) * 240 / (maxLength))/100;
		var s = 1;
		var l = 0.5;

		//return "hsl(" + h*100 + ","+s*100+"%,"+l+"%)";
		rgb = hslToRgb(h, s, l);
		//console.log(n+"/"+maxLength, h, s, l, rgb2hex(rgb[0],rgb[1],rgb[2]))
		return rgb2hex(rgb[0],rgb[1],rgb[2]);
	}
	function hslToRgb(h, s, l){
	    var r, g, b;

	    if(s == 0){
	        r = g = b = l; // achromatic
	    } else{
	        function hue2rgb(p, q, t){
	            if(t < 0) t += 1;
	            if(t > 1) t -= 1;
	            if(t < 1/6) return p + (q - p) * 6 * t;
	            if(t < 1/2) return q;
	            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
	            return p;
	        }

	        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	        var p = 2 * l - q;
	        r = hue2rgb(p, q, h + 1/3);
	        g = hue2rgb(p, q, h);
	        b = hue2rgb(p, q, h - 1/3);
	    }

	    return [r * 255, g * 255, b * 255];
	}
	function rgb2hex(r, g, b) {
		var hex = [Number(r).toString(16), Number(g).toString(16), Number(b).toString(16)];
		for (var i in hex) {
			if (hex[i].indexOf(".") != -1) {
				hex[i] = hex[i].substr(0, hex[i].indexOf("."));
			}
			if (hex[i].length < 2) {
				hex[i] = "0"+hex[i];
			}
		}
		return "#"+hex[0]+hex[1]+hex[2];
	}

	// Helper functions for plot_initializeAxis() and plot_drawAxis()
	function plot_axisName( name, axisIndex ) {
		return ['x','y','z'][axisIndex] + name;
	}

	function plot_constVecWithAxisValue( otherValue, axisValue, axisIndex ) {
		var result = [otherValue, otherValue, otherValue];
		result[axisIndex] = axisValue;
		return result;
	}

	// Used to make 2d elements visible
	function plot_makeSolid(selection, color) {
		selection.append("appearance")
		  .append("material")
		     .attr("diffuseColor", color||"black")
		return selection;
	}

	// Initialize the axes lines and labels.
	function plot_initializePlot(scene, dataRanges, axisScales) {
		scene.select("orthoviewpoint")
			 //.attr( "centerOfRotation", [dataRanges[0][1], dataRanges[1][1], dataRanges[2][1]])
			 //.attr( "fieldOfView", [-5, -5, 15, 15])
			 //.attr( "orientation", [-0.5, 1, 0.2, 1.12*Math.PI/4])
			 //.attr( "position", [8, 4, 15])

		plot_initializeAxis(0, dataRanges[0], axisScales[0], scene);
		plot_initializeAxis(1, dataRanges[1], axisScales[1], scene);
		plot_initializeAxis(2, dataRanges[2], axisScales[2], scene);
	}

	function plot_initializeAxis( axisIndex, dataRange, axisScale, scene )
	{
		var key = axisKeys[axisIndex];
		plot_drawAxis( axisIndex, dataRange, axisScale, key, scene );

		var scaleMin = axisRange[0];
		var scaleMax = axisRange[1];

		// the axis line
		var newAxisLine = scene.append("transform")
		     .attr("class", plot_axisName("Axis", axisIndex))
		     .attr("rotation", ([[0,0,0,0],[0,0,1,Math.PI/2],[0,1,0,-Math.PI/2]][axisIndex]))
		  .append("shape")
		newAxisLine
		  .append("appearance")
		  .append("material")
		    .attr("emissiveColor", "lightgray")
		newAxisLine
		  .append("polyline2d")
		     // Line drawn along y axis does not render in Firefox, so draw one
		     // along the x axis instead and rotate it (above).
		    .attr("lineSegments", "0 0," + scaleMax + " 0")

		// axis labels
		var newAxisLabel = scene.append("transform")
		   .attr("class", plot_axisName("AxisLabel", axisIndex))
		   .attr("translation", plot_constVecWithAxisValue( 0, scaleMin + 1.1 * (scaleMax-scaleMin), axisIndex ))

		var newAxisLabelShape = newAxisLabel
		 .append("billboard")
		   .attr("axisOfRotation", "0 0 0") // face viewer
		 .append("shape")
		 .call(plot_makeSolid)

		var labelFontSize = 0.6;

		newAxisLabelShape
		 .append("text")
		   .attr("class", plot_axisName("AxisLabelText", axisIndex))
		   .attr("solid", "true")
		   .attr("string", key)
		.append("fontstyle")
		   .attr("size", labelFontSize)
		   .attr("family", "SANS")
		   .attr("justify", "END MIDDLE" )
	}

	// Assign key to axis, creating or updating its ticks, grid lines, and labels.
	function plot_drawAxis( axisIndex, dataRange, axisScale, key, scene ) {
		var scale = d3.scale.linear()
			.domain( axisScale )
			.range( axisRange )
		    //.range( [0, axisRange[1]-axisRange[0]] )
		  //.domain( [-5,5] ) // demo data range
		  //.range( [0, 10] )

		scales[axisIndex] = scale;

		var numTicks = 8;
		var numTicksScale = Math.ceil((dataRange[1]-dataRange[0]) / (axisRange[1]-axisRange[0]) * numTicks)
		var tickSize = 0.1;
		var tickFontSize = 0.5;

		
		/*//COMMENTED OUT: a huge number of ticks takes too much performance

		// ticks along each axis
		var ticks = scene.selectAll( "."+plot_axisName("Tick", axisIndex) )
		   .data( scale.ticks( numTicksScale ));
		var newTicks = ticks.enter()
		  .append("transform")
		    .attr("class", plot_axisName("Tick", axisIndex));
		newTicks.append("shape").call(plot_makeSolid)
		  .append("box")
		    .attr("size", tickSize + " " + tickSize + " " + tickSize);
		// enter + update
		ticks.attr("translation", function(tick) { 
		     return plot_constVecWithAxisValue( 0, scale(tick), axisIndex ); })
		ticks.exit().remove();

		// tick labels
		var tickLabels = ticks.selectAll("billboard shape text")
		  .data(function(d) { return [d]; });
		var newTickLabels = tickLabels.enter()
		  .append("billboard")
		     .attr("axisOfRotation", "0 0 0")     
		  .append("shape")
		  .call(plot_makeSolid)
		newTickLabels.append("text")
		  .attr("string", scale.tickFormat(10))
		  .attr("solid", "true")
		  .append("fontstyle")
		    .attr("size", tickFontSize)
		    .attr("family", "SANS")
		    .attr("justify", "END MIDDLE" );
		tickLabels // enter + update
		  .attr("string", scale.tickFormat(10))
		tickLabels.exit().remove();*/
		
	
		// base grid lines
		if (axisIndex==0 || axisIndex==2) {

		  var gridLines = scene.selectAll( "."+plot_axisName("GridLine", axisIndex))
		     .data(scale.ticks( numTicks ));
		  gridLines.exit().remove();
		  
		  var newGridLines = gridLines.enter()
		    .append("transform")
		      .attr("class", plot_axisName("GridLine", axisIndex))
		      .attr("rotation", axisIndex==0 ? [0,1,0, -Math.PI/2] : [0,0,0,0])
		    .append("shape")

		  newGridLines.append("appearance")
		    .append("material")
		      .attr("emissiveColor", "gray")
		  newGridLines.append("polyline2d");

		  gridLines.selectAll("shape polyline2d").attr("lineSegments", "0 0, " + axisRange[1] + " 0")

		  gridLines.attr("translation", axisIndex==0
		        ? function(d) { return scale(d) + " 0 0"; }
		        : function(d) { return "0 0 " + scale(d); }
		    )
		}  
	}

	// Update the data points (spheres) and stems.
	function plot_plotData( rows, labelCount, scene ) {
		if (!rows) {
		 console.log("no rows to plot.")
		 return;
		}

		var x = scales[0], y = scales[1], z = scales[2];

		// Draw a sphere at each x,y,z coordinate.
		var datapoints = scene.selectAll(".datapoint").data( rows );
		datapoints.exit().remove()

		var newDatapoints = datapoints.enter()
		  .append("transform")
		    .attr("class", "datapoint")
		    .attr("scale", [sphereRadius, sphereRadius, sphereRadius])
		  .append("shape");
		newDatapoints
		  .append("appearance")
		  .append("material");
		newDatapoints
		  .append("sphere")
		   // Does not work on Chrome; use transform instead
		   //.attr("radius", sphereRadius)

		datapoints.selectAll("shape appearance material")
		    .attr("diffuseColor", function(d) {return getColorAtScalar(d.label, labelCount);} )

		datapoints.attr("translation", function(row) { 
		      return x(row[axisKeys[0]]) + " " + y(row[axisKeys[1]]) + " " + z(row[axisKeys[2]])})
	}

	function plot_updateData(data, labelCount, x3d, scene) {
		//var tryAgainInterval = setInterval( function() {
			if ( x3d.node() && x3d.node().runtime ) {
			  plot_plotData(data, labelCount, scene);
			  //clearInterval(tryAgainInterval);
			} else {
			  console.log('x3d not ready.');
			}
		//}, 500 );
	}
});