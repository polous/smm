#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import cherrypy
import jinja2
import numpy as np
from exceptions import Exception
import json

import uiinc.ExecThread
import ui_definition

############### CONFIG #################
CONFIG_CHERRYPY_CONF = "appserver.conf"
CONFIG_WWWDOC_DIR = "wwwdoc"
########################################

############# LOAD CONFIG ##############
CONFIG_WWWDOC_DIR_ABS = os.path.abspath(os.path.join(os.path.dirname(__file__), CONFIG_WWWDOC_DIR))
########################################


jinja2_env = jinja2.Environment(loader=jinja2.FileSystemLoader(CONFIG_WWWDOC_DIR_ABS), variable_start_string="{%P", variable_end_string="P%}")

class AppServerException(Exception):
    pass

class Controller(object):
	def get_template(self, tpl, data=None):
		try :
			if not(data is None):
				return jinja2_env.get_template(tpl).render(**data)
			else:
				return jinja2_env.get_template(tpl).render()
		except Exception as e:
			raise AppServerException("illegal template: '%s'" % (templateFile,)), None, sys.exc_info()[2]

class AppController(Controller):

    _cp_config = {
    	'tools.sessions.on': True,
    	'tools.staticdir.root': CONFIG_WWWDOC_DIR_ABS
    }

    def __init__(self, stdoutRedirector):
        self.stdoutRedirector = stdoutRedirector

    @cherrypy.expose
    def index(self):
        jsConfig_uiparams_options = {}
        jsConfig_uiparams_templates = []
        for uiParamOptionGroupName in ui_definition.UIParameterOptions:
            uiParamOptionGroup = ui_definition.UIParameterOptions[uiParamOptionGroupName]
            jsConfig_uiparams_options[uiParamOptionGroupName] = {
                "name": uiParamOptionGroupName,
                "label": uiParamOptionGroup["label"],
                "sortIndex": uiParamOptionGroup["sortIndex"] if "sortIndex" in uiParamOptionGroup else 0,
                "options": {}
            };
            for uiParamOptionName in uiParamOptionGroup["options"]:
                uiParamOption = uiParamOptionGroup["options"][uiParamOptionName]
                jsConfig_uiparams_options[uiParamOptionGroupName]["options"][uiParamOptionName] = {
                    "name": uiParamOptionName,
                    "label": uiParamOption["label"],
                    "sortIndex": uiParamOption["sortIndex"] if "sortIndex" in uiParamOption else 0,
                    "params": {}
                };
                if "params" in uiParamOption:
                    for uiParamOptionParamName in uiParamOption["params"]:
                        uiParamOptionParam = uiParamOption["params"][uiParamOptionParamName]
                        jsConfig_uiparams_options[uiParamOptionGroupName]["options"][uiParamOptionName]["params"][uiParamOptionParamName] = {
                            "name": uiParamOptionParamName,
                            "label": uiParamOptionParam["label"],
                            "sortIndex": uiParamOptionParam["sortIndex"] if "sortIndex" in uiParamOptionParam else 0,
                            "template": uiParamOptionParam["template"],
                            "view": uiParamOptionParam["view"]
                        }
                        if not(uiParamOptionParam["template"] in jsConfig_uiparams_templates):
                            jsConfig_uiparams_templates.append(uiParamOptionParam["template"])

        jsConfig_uiparams = {}
        for uiParamGroupName in ui_definition.UIParameters:
            uiParamGroup = ui_definition.UIParameters[uiParamGroupName]
            jsConfig_uiparams[uiParamGroupName] = {
                "name": uiParamGroupName,
                "label": uiParamGroup["label"],
                "label-css": uiParamGroup["label-css"] if "label-css" in uiParamGroup else None,
                "sortIndex": uiParamGroup["sortIndex"] if "sortIndex" in uiParamGroup else 0,
                "optionGroup": uiParamGroup["optionGroup"],
                "optionCountMin": uiParamGroup["optionCountMin"] if ("optionCountMin" in uiParamGroup) else None,
                "optionCountMax": uiParamGroup["optionCountMax"] if ("optionCountMax" in uiParamGroup) else None,

                "default": uiParamGroup["default"] if ("default" in uiParamGroup) else []
            }


        jsConfig_eventInfo = {}
        jsConfig_eventInfo_templates = []
        for uiEventInfoName in ui_definition.UIEventInfo:
            uiEventInfo = ui_definition.UIEventInfo[uiEventInfoName]
            if not(ui_definition.UIEventInfo[uiEventInfoName] in jsConfig_eventInfo_templates):
                jsConfig_eventInfo_templates.append(uiEventInfo)
            jsConfig_eventInfo[uiEventInfoName] = uiEventInfo


        jsConfig = "UIPARAMS_LOAD_TEMPLATES = " + json.dumps(jsConfig_uiparams_templates) + ";"
        jsConfig += "UIPARAMS_OPTIONS = " + json.dumps(jsConfig_uiparams_options) + ";"
        jsConfig += "UIPARAMS = " + json.dumps(jsConfig_uiparams) + ";"
        jsConfig += "EVENTINFO_LOAD_TEMPLATES = " + json.dumps(jsConfig_eventInfo_templates) + ";"
        jsConfig += "EVENTINFO = " + json.dumps(jsConfig_eventInfo) + ";"

        yield self.get_template("index.pyhtml", {
            "PYTHON_CONFIG": jsConfig
        })

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def execStart(self):
        input_json = cherrypy.request.json

        if not("exec" in cherrypy.session):
            cherrypy.session["exec"] = {
                "counter": 0,
                "runs": {}
            }

        runId = cherrypy.session["exec"]["counter"]

        thread = uiinc.ExecThread.ExecThread(input_json)
        thread.daemon = True
        stdoutBuffer = self.stdoutRedirector.capture(thread)
        thread.start()

        cherrypy.session["exec"]["runs"][runId] = {
            "thread": thread,
            "stdoutBuffer": stdoutBuffer
        }
        cherrypy.session["exec"]["counter"] += 1

        return {"status": "ok", "runId": runId}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def execUpdate(self, runId):
        runId = int(runId)
        if not("exec" in cherrypy.session and runId in cherrypy.session["exec"]["runs"]):
            return {"status": "error", "errorMessage": "illegal runId"}

        run = cherrypy.session["exec"]["runs"][runId]
        stdoutBufferUpdate = run["stdoutBuffer"].take()
        
        if (run["thread"].isAlive()):
            # still running
            return {
                "status": "ok",
                "execStatus": "running",
                "update": stdoutBufferUpdate
            }
        else:
            # finished
            events = None
            data = None
            csv = None
            tsne = None
            dataCount = None

            # retrieve events
            if not(run["thread"].result["events"] is None):
                events = []
                for event in run["thread"].result["events"]:
                    events.append(event.to_JSONCompatibleObject())

            # retrieve data
            if not(run["thread"].result["data"] is None):
                dataCount = run["thread"].result["dataCount"]
                rawData = np.asarray(run["thread"].result["data"]["data"])
                axisScales = run["thread"].result["data"]["ranges"]

                minima = np.min(rawData, axis=0)
                maxima = np.max(rawData, axis=0)
                ranges = []
                for i in range(minima.shape[0]):
                    ranges.append((minima[i], maxima[i]))
                data = {
                    "data": rawData.tolist(),
                    "ranges": ranges,
                    "axisScales": axisScales,
                    "labelCount": 0 if events is None else len(events),
                }

            # retrieve tsne plot
            if "tsne" in run["thread"].result and not(run["thread"].result["tsne"] is None):
                tsne = run["thread"].result["tsne"]

            # retrieve csv
            if "csv" in run["thread"].result and not(run["thread"].result["csv"] is None):
                csv = run["thread"].result["csv"]

            # clear session
            del cherrypy.session["exec"]["runs"][runId]

            # send output
            if not(events is None):
                return {
                    "status": "ok",
                    "execStatus": "finished",
                    "update": stdoutBufferUpdate,
                    "events": events,
                    "data": data,
                    "csv": csv,
                    "tsne": tsne,
                    "dataCount": dataCount
                }
            else:
                return {
                    "status": "ok",
                    "execStatus": "error",
                    "execError": "run didn't finish correctly",
                    "update": stdoutBufferUpdate,
                }



    @cherrypy.expose
    def graph(self):
        yield self.get_template("graph.pyhtml")

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def data(self):
        import random
        labelCount=3
        data = []
        for i in range(30):
            data.append([random.randint(-10,10), random.randint(-10,10), random.randint(-10,10), random.randint(0,labelCount-1)])
        data = np.asarray(data)
        if False:
        	data = np.asarray([
        		[0,3,5, 0],
        		[1,3,3, 1],
        		[0,5,2, 2],
        	])

    	minima = np.min(data, axis=0)
    	maxima = np.max(data, axis=0)
    	ranges = []
    	for i in range(minima.shape[0]):
    		ranges.append((minima[i], maxima[i]))
    	axisScales = [[-20,20],[-20,20],[-20,20]]
    	return {
    		"data": data.tolist(),
    		"labelCount": labelCount,
    		"ranges": ranges,
    		"axisScales": axisScales,
	    }


if __name__ == '__main__':
    """ capture stdout """
    # define helpers
    import threading
    class StdoutBuffer(object):
        def __init__(self):
            self._buffer = ""
            self._lock = threading.Lock()

        def write(self, value):
            self._lock.acquire()
            self._buffer += value
            self._lock.release()

        def take(self):
            self._lock.acquire()
            buffer = self._buffer
            self._buffer = ""
            self._lock.release()
            return buffer

    class ThreadBasedStdoutRedirector(object):
        def __init__(self, stdout):
            self._stdout = stdout
            self._buffers = {}
            self._lock = threading.Lock()

        def capture(self, thread):
            self._lock.acquire()
            buffer = StdoutBuffer()
            self._buffers[thread] = buffer
            self._lock.release()
            return buffer

        def write(self, value):
            self._lock.acquire()
            currentThread = threading.currentThread()
            if currentThread in self._buffers:
                self._buffers[currentThread].write(value)
            else:
                self._stdout.write(value)
            self._lock.release()
    # capture stdout
    stdoutRedirector = ThreadBasedStdoutRedirector(sys.stdout)
    sys.stdout = stdoutRedirector

    """ start web server """
    cherrypy.quickstart(AppController(stdoutRedirector), config=os.path.join(os.path.dirname(__file__), CONFIG_CHERRYPY_CONF))