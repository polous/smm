import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventLocationExtractor
from inc import algos
import numpy as np

class Location(EventLocationExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		# fit gaussians
		timeSpaceData = np.empty((len(contentCluster), 2))
		for i in range(len(contentCluster)):
			timeSpaceData[i][0] = contentCluster[i].GeoLocation.latitude
			timeSpaceData[i][1] = contentCluster[i].GeoLocation.longitude
		gauss_means, gauss_stds = algos.extract_event_gaussians(timeSpaceData)

		event_latitude_center = gauss_means[0]
		event_longitude_center = gauss_means[1]
		event_latitude_start = event_latitude_center - (gauss_stds[0]/2)
		event_latitude_end = event_latitude_center + (gauss_stds[0]/2)
		event_longitude_start = event_longitude_center - (gauss_stds[1]/2)
		event_longitude_end = event_longitude_center + (gauss_stds[1]/2)

		return {
			"center": (event_latitude_center, event_longitude_center),
			"bbox": {
				"latitude": (event_latitude_start, event_latitude_end),
				"longitude": (event_longitude_start, event_longitude_end)
			}
		}

	def __str__(self):
		return "Location"